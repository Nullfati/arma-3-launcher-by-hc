﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Threading;

using Hardcodet.Wpf.TaskbarNotification;

using System.IO;

using Microsoft.Win32;

namespace Arma_3_Launcher_by_HC
{
    /// <summary>
    /// Логика взаимодействия для FirstChooseArmaPath.xaml
    /// </summary>
    public partial class FirstChooseArmaPath : Window
    {
        public FirstChooseArmaPath()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (File.Exists(Environment.CurrentDirectory + "\\Auto updater of Launcher.exe"))
                    File.Delete(Environment.CurrentDirectory + "\\Auto updater of Launcher.exe");

                this.Topmost = true;

                if (File.Exists("arma3.exe"))
                {
                    Properties.Settings.Default.ArmaPath = Environment.CurrentDirectory + "\\arma3.exe";
                    Properties.Settings.Default.Save();
                    this.Close();
                }

                if ((string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Bohemia Interactive\arma 3", "main", null) != null)
                {
                    Properties.Settings.Default.ArmaPath = (string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Bohemia Interactive\arma 3", "main", null) + "\\arma3.exe";
                    Properties.Settings.Default.Save();
                    this.Close();
                }
            }
            catch (ThreadAbortException)
            {
                throw;
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Ошибка: " + ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ChoosePath_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog OPF = new Microsoft.Win32.OpenFileDialog();
            OPF.Filter = "Arma 3|arma3.exe";
            if (OPF.ShowDialog() == true)
            {
                //MessageBox.Show(OPF.FileName);
                Properties.Settings.Default.ArmaPath = OPF.FileName;
                Properties.Settings.Default.Save();
                this.Close();
            }
        }

        private void DragWindow_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void CloseButton_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            //Tray.Dispose();

            ThreadStart ts = delegate()
            {
                Dispatcher.BeginInvoke((Action)delegate()
                {
                    Application.Current.Shutdown();
                });
            };
            Thread t = new Thread(ts);
            t.Start();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (Properties.Settings.Default.ArmaPath == "")
            {
                ThreadStart ts = delegate()
                {
                    Dispatcher.BeginInvoke((Action)delegate()
                    {
                        Application.Current.Shutdown();
                    });
                };
                Thread t = new Thread(ts);
                t.Start();
            }
        }
    }
}
