﻿using BytesRoad.Net.Ftp;
using System;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using System.Net;
using System.Collections.Generic;

namespace Arma_3_Launcher_by_HC
{
    /// <summary>
    /// Логика взаимодействия для DownloadWindow.xaml
    /// </summary>
    public partial class DownloadWindow : Window
    {
        private Thread FTP = null;
        private Thread DownloadFromFTP = null;
        public bool Finish = false;
        private int i = 0;
        private int h = 0;
        private long FileSizeServer = 0;

        private int countTimeOutException = 0;

        private string localFile = string.Empty;

        private FtpClient client = new FtpClient();

        //Задаём параметры клиента
        private int TimeoutFTP = 600000; //Таймаут

        private string FTP_SERVER = "cramerstation.ru";
        private int FTP_PORT = 21;
        private string FTP_USER = "launcher";   //"drcramer", "AbvzH7EV"
        private string FTP_PASSWORD = "flAjzm1eCj";     //"anonymous", ""

        //62.109.13.179
        //launcher
        //flAjzm1eCj

        public DownloadWindow()
        {
            InitializeComponent();
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            client.Abort(TimeoutFTP);
            //Thread.Sleep(TimeoutFTP);
            client.Disconnect(TimeoutFTP);
            FTP.Abort();
            
            this.Close();
        }

        private void DragWindow_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void MinimizeButton_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (Properties.Settings.Default.OnMinimizeGoInTray == false)
                this.WindowState = System.Windows.WindowState.Minimized;
            else
                Visibility = Visibility.Hidden;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            FTP = new Thread(new ThreadStart(FTPDownload));
            FTP.IsBackground = true;
            FTP.SetApartmentState(ApartmentState.STA);
            FTP.Start();
        }

        private void ProgressBarFile()
        {
            try
            {
                System.IO.FileInfo file = new System.IO.FileInfo(localFile);
                //Int64 FileSizeBefore = 0;
                while (true)
                {
                    file = new System.IO.FileInfo(localFile);
                    if (File.Exists(localFile))
                    {
                        Dispatcher.Invoke((Action)(() =>
                        {
                            FileProgressBar.Value = file.Length;
                            FileProgressBarPercent.Text = Math.Round(((FileProgressBar.Value / FileSizeServer) * 100), 0) + "%";
                        }));
                        Thread.Sleep(1);
                    }
                }
            }
            catch (ThreadAbortException)
            {
                throw;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DownloadSpeedFunction()
        {
            try
            {
                System.IO.FileInfo file = new System.IO.FileInfo(localFile);
                Int64 FileSizeBefore = 0;
                Thread.Sleep(1000);
                while (true)
                {
                    if (File.Exists(localFile))
                    {
                        file = new System.IO.FileInfo(localFile);
                        Dispatcher.Invoke((Action)(() =>
                        {
                            //if (Math.Round((file.Length - FileSizeBefore) / 1048576.0, 2) < 0.0)
                            //    DownloadSpeed.Text = "Скорость: 0 MB/s";
                            //else
                            //    DownloadSpeed.Text = "Скорость: " + Math.Round((file.Length - FileSizeBefore) / 1048576.0, 2) + " MB/s";

                            if (Math.Round((file.Length - FileSizeBefore) / 1048576.0, 2) < 1.0)
                            {
                                if (Math.Round((file.Length - FileSizeBefore) / 1024.0, 2) < 0.0)
                                    DownloadSpeed.Text = "Скорость: 0 MB/s";
                                else
                                    DownloadSpeed.Text = "Скорость: " + Math.Round((file.Length - FileSizeBefore) / 1024.0, 2) + " KB/s";
                            }
                            else
                            {
                                if (Math.Round((file.Length - FileSizeBefore) / 1048576.0, 2) < 0.0)
                                    DownloadSpeed.Text = "Скорость: 0 MB/s";
                                else
                                    DownloadSpeed.Text = "Скорость: " + Math.Round((file.Length - FileSizeBefore) / 1048576.0, 2) + " MB/s";
                            }



                            if ((Math.Round(FileSizeServer / 1048576.0, 2) < 1.0))
                            {
                                if (Math.Round(file.Length / 1024.0, 2) < 0.0)
                                    SizeOfFile.Text = "Загружено: 0 KB/" + Math.Round(FileSizeServer / 1024.0, 2) + " KB";
                                else
                                    SizeOfFile.Text = "Загружено: " + Math.Round(file.Length / 1024.0, 2) + " KB/" + Math.Round(FileSizeServer / 1024.0, 2) + " KB";
                            }
                            else
                            {
                                if (Math.Round(file.Length / 1048576.0, 2) < 0.0)
                                    SizeOfFile.Text = "Загружено: 0 MB/" + Math.Round(FileSizeServer / 1048576.0, 2) + " MB";
                                else
                                    SizeOfFile.Text = "Загружено: " + Math.Round(file.Length / 1048576.0, 2) + " MB/" + Math.Round(FileSizeServer / 1048576.0, 2) + " MB";
                            }

                            //if (Math.Round(file.Length / 1048576.0, 2) < 0.0)
                            //    SizeOfFile.Text = "Загружено: 0 MB/" + Math.Round(FileSizeServer / 1048576.0, 2) + " MB";
                            //else
                            //    SizeOfFile.Text = "Загружено: " + Math.Round(file.Length / 1048576.0, 2) + " MB/" + Math.Round(FileSizeServer / 1048576.0, 2) + " MB";

                            //if ((Math.Round(FileSizeServer / 1048576.0, 2) > 1024.0))
                            //{
                            //    if (Math.Round(file.Length / 1073741824.0, 2) < 0.0)
                            //        SizeOfFile.Text = "Загружено: 0 GB/" + Math.Round(FileSizeServer / 1073741824.0, 2) + " GB";
                            //    else
                            //        SizeOfFile.Text = "Загружено: " + Math.Round(file.Length / 1073741824.0, 2) + " GB/" + Math.Round(FileSizeServer / 1073741824.0, 2) + " GB";
                            //}
                            //else
                            //{
                            //    if (Math.Round(file.Length / 1048576.0, 2) < 0.0)
                            //        SizeOfFile.Text = "Загружено: 0 MB/" + Math.Round(FileSizeServer / 1048576.0, 2) + " MB";
                            //    else
                            //        SizeOfFile.Text = "Загружено: " + Math.Round(file.Length / 1048576.0, 2) + " MB/" + Math.Round(FileSizeServer / 1048576.0, 2) + " MB";
                            //}
                        }));
                        FileSizeBefore = file.Length;
                    }
                    else
                    {
                        Dispatcher.Invoke((Action)(() =>
                        {
                            DownloadSpeed.Text = "Скорость: 0 MB/s";
                            SizeOfFile.Text = "Загружено: 0 MB/" + Math.Round(FileSizeServer / 1048576.0, 1) + " MB";
                        }));
                    }
                
                    Thread.Sleep(1000);
                }
            }
            catch (ThreadAbortException)
            {
                throw;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Download()
        {
            string tmpStDown = "";
            string stPath = "";

            tmpStDown = GeneralWindow.stDown[i].Replace("\\", "/");

            if(GeneralWindow.WhatModDownload == "arma_3")
                stPath = Properties.Settings.Default.ArmaPath.Replace("arma3.exe", "");
            else
                stPath = Properties.Settings.Default.ArmaPath.Replace("arma3.exe", "") + GeneralWindow.folders[h];

            if (!(Directory.Exists(stPath)))
                Directory.CreateDirectory(stPath);

            Dispatcher.Invoke((Action)(() =>
            {
                NowDownloadFile.Text = tmpStDown;
            }));

            Thread DownloadSpeedThread = new Thread(new ThreadStart(DownloadSpeedFunction));

            Thread ProgressBarFileThread = new Thread(new ThreadStart(ProgressBarFile));

            try
            {
                localFile = stPath + GeneralWindow.stDown[i].Remove(0, GeneralWindow.folders[h].Length);
                string test = stPath + GeneralWindow.stDown[i].Remove(0, GeneralWindow.folders[h].Length);

                localFile.Replace("\\\\", "\\");

                if (File.Exists(localFile))
                    File.SetAttributes(localFile, FileAttributes.Normal);

                //tmpStDown
                if (GeneralWindow.WhatModDownload == "arma_3")
                    tmpStDown = tmpStDown.Replace((tmpStDown.Split(new Char[] { '/' }))[0], "arma_3");
                FtpWebRequest request = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + FTP_SERVER + "/mods/" + GeneralWindow.WhatModDownload + "/Mod/" + tmpStDown));
                //request.UseBinary = true;
                request.KeepAlive = false;
                request.Credentials = new NetworkCredential(FTP_USER, FTP_PASSWORD);
                request.Method = WebRequestMethods.Ftp.GetFileSize;

                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                FileSizeServer = response.ContentLength;
                response.Close();

                Dispatcher.Invoke((Action)(() =>
                {
                    FileProgressBar.Maximum = FileSizeServer;
                    FileProgressBar.Value = 0;
                    //SizeOfFile.Text = "Загружено: 0 MB/0 MB";

                    if ((Math.Round(FileSizeServer / 1048576.0, 2) < 1.0))
                        SizeOfFile.Text = "Загружено: 0 KB/" + Math.Round(FileSizeServer / 1024.0, 2) + " KB";
                    else
                        SizeOfFile.Text = "Загружено: 0 MB/" + Math.Round(FileSizeServer / 1048576.0, 2) + " MB";

                    //if ((Math.Round(FileSizeServer / 1048576.0, 2) > 1024.0))
                    //    SizeOfFile.Text = "Загружено: 0 GB/" + Math.Round(FileSizeServer / 1073741824.0, 2) + " GB";
                    //else
                    //    SizeOfFile.Text = "Загружено: 0 MB/" + Math.Round(FileSizeServer / 1048576.0, 2) + " MB";

                    //SizeOfFile.Text = "Загружено: 0 MB/" + Math.Round(FileSizeServer / 1048576.0, 2) + " MB";
                }));

                //long test = this.GetItemsFromFtp();

                DownloadSpeedThread.IsBackground = true;
                DownloadSpeedThread.Start();

                ProgressBarFileThread.IsBackground = true;
                ProgressBarFileThread.Start();

                client.GetFile(TimeoutFTP, localFile, "mods/" + GeneralWindow.WhatModDownload + "/Mod/" + tmpStDown);

                ProgressBarFileThread.Abort();
                ProgressBarFileThread = null;

                DownloadSpeedThread.Abort();
                DownloadSpeedThread = null;
            }
            catch (DirectoryNotFoundException)
            {
                ProgressBarFileThread.Abort();
                ProgressBarFileThread = null;

                DownloadSpeedThread.Abort();
                DownloadSpeedThread = null;

                string remoteFile = tmpStDown;
                string localFile = @stPath + GeneralWindow.stDown[i].Remove(0, GeneralWindow.folders[h].Length);
                // Let the user know that the directory did not exist.
                string[] arrTmpPath;
                string[] arrTmpFold;
                string tmpPath = "";
                arrTmpPath = remoteFile.Split(new char[] { '/' });
                arrTmpFold = localFile.Split(new char[] { '\\' });

                for (int k = 0; k < arrTmpFold.Length - 2; k++)
                {
                    if (arrTmpFold[k].Length != 0)
                    {
                        tmpPath += arrTmpFold[k] + "\\";
                    }
                }
                //tmpPath += arrTmpPath[1] + '\\';
                //MessageBox.Show(localFile + arrTmpPath[0]);
                Directory.CreateDirectory(tmpPath + arrTmpPath[arrTmpPath.Length - 2]);

                Download();
            }
            catch (BytesRoad.Net.Ftp.FtpAbortedException)
            {
                ProgressBarFileThread.Abort();
                ProgressBarFileThread = null;

                DownloadSpeedThread.Abort();
                DownloadSpeedThread = null;

                Thread.Sleep(10);
            }
            catch (ThreadAbortException)
            {
                ProgressBarFileThread.Abort();
                ProgressBarFileThread = null;

                DownloadSpeedThread.Abort();
                DownloadSpeedThread = null;

                throw;
            }
            catch (BytesRoad.Net.Ftp.FtpTimeoutException ex)
            {
                ProgressBarFileThread.Abort();
                ProgressBarFileThread = null;

                DownloadSpeedThread.Abort();
                DownloadSpeedThread = null;

                countTimeOutException++;

                Thread.Sleep(10);
            }
            catch (InvalidOperationException ex)
            {
                ProgressBarFileThread.Abort();
                ProgressBarFileThread = null;

                DownloadSpeedThread.Abort();
                DownloadSpeedThread = null;

                if (ex.Message == "Connection with server is absent.")
                {
                    client.Connect(TimeoutFTP, FTP_SERVER, FTP_PORT);
                    client.Login(TimeoutFTP, FTP_USER, FTP_PASSWORD);
                    client.PassiveMode = true;
                }
                else
                    MessageBox.Show("Ошибка 1: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception ex)
            {
                ProgressBarFileThread.Abort();
                ProgressBarFileThread = null;

                DownloadSpeedThread.Abort();
                DownloadSpeedThread = null;

                MessageBox.Show("Ошибка 2: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void FTPDownload()
        {
            double NeedDownload = 0.0;

            for (int d = 0; d < GeneralWindow.stDown.Length; d++)
            {
                if (GeneralWindow.stDown[d] == null) GeneralWindow.stDown[d] = "";
                else NeedDownload++;
            }

            //client.PassiveMode = true; //Включаем пассивный режим.

            try
            {
                client.Connect(TimeoutFTP, FTP_SERVER, FTP_PORT);
                client.Login(TimeoutFTP, FTP_USER, FTP_PASSWORD);
                client.PassiveMode = true;

                string tmpStDown = string.Empty;

                for (; h < GeneralWindow.folders.Count; h++)
                {
                    Dispatcher.Invoke((Action)(() =>
                    {
                        GeneralProgressBar.Maximum = NeedDownload;
                        NumberOfFiles.Text = "Загружено 0 из " + NeedDownload + " файлов";
                    }));
                    string[] abc = GeneralWindow.stDown[i].Split(new Char[] { '\\' });

                    for (; GeneralWindow.folders[h] == abc[0] && i < GeneralWindow.stDown.Length; )
                    {
                        if (GeneralWindow.stDown[i].Length != 0)
                        {
                            tmpStDown = GeneralWindow.stDown[i].Replace("\\", "/");

                            DownloadFromFTP = new Thread(new ThreadStart(Download));
                            DownloadFromFTP.IsBackground = true;
                            DownloadFromFTP.Start();

                            DownloadFromFTP.Join();

                            i++;

                            Dispatcher.Invoke((Action)(() =>
                            {
                                GeneralProgressBar.Value++;

                                GeneralProgressBarPercent.Text = Math.Round(((i / NeedDownload) * 100), 0) + "%";
                                NumberOfFiles.Text = "Загружено " + i + " из " + NeedDownload + " файлов";
                            }));

                            abc = GeneralWindow.stDown[i].Split(new Char[] { '\\' });
                        }
                    }
                }
                
            }
            catch (BytesRoad.Net.Ftp.FtpAbortedException)
            {
                Thread.Sleep(10);
            }
            catch (ThreadAbortException)
            {
                throw;
            }
            catch (BytesRoad.Net.Ftp.FtpTimeoutException ex)
            {
                MessageBox.Show("Ошибка: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Thread.Sleep(10);
                FTPDownload();
            }
            catch (InvalidOperationException ex)
            {
                if (ex.Message == "Connection with server is absent.")
                {
                    //client.Connect(TimeoutFTP, FTP_SERVER, FTP_PORT);
                    //client.Login(TimeoutFTP, FTP_USER, FTP_PASSWORD);
                    //client.PassiveMode = true;

                    FTPDownload();
                }
                else
                    MessageBox.Show("Ошибка: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            if (!Directory.Exists(Properties.Settings.Default.ArmaPath.Replace("arma3.exe", "") + "version_of_mod") && GeneralWindow.WhatModDownload !="arma_3")
            {
                Directory.CreateDirectory(Properties.Settings.Default.ArmaPath.Replace("arma3.exe", "") + "version_of_mod");
                client.GetFile(TimeoutFTP, Properties.Settings.Default.ArmaPath.Replace("arma3.exe", "") + "version_of_mod\\" + GeneralWindow.WhatModDownload + ".txt", "mods/" + GeneralWindow.WhatModDownload + "/version_of_mod/" + GeneralWindow.WhatModDownload + ".txt");
            }
            else if (GeneralWindow.WhatModDownload != "arma_3")
                client.GetFile(TimeoutFTP, Properties.Settings.Default.ArmaPath.Replace("arma3.exe", "") + "version_of_mod\\" + GeneralWindow.WhatModDownload + ".txt", "mods/" + GeneralWindow.WhatModDownload + "/version_of_mod/" + GeneralWindow.WhatModDownload + ".txt");

            client.Abort(TimeoutFTP);
            client.Disconnect(TimeoutFTP);

            GeneralWindow.stDown = null;
            GeneralWindow.folders.Clear();

            if(countTimeOutException == 0)
                Finish = true;


            Dispatcher.Invoke((Action)(() =>
            {
                this.Close();
            }));
        }
    }
}