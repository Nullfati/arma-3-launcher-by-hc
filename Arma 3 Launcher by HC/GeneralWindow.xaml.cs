﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Reflection;
using System.IO;
using System.Net;
using System.Diagnostics; 
using System.Security.Cryptography;
using System.Windows.Threading;
using System.Threading;
using System.Globalization;
using System.Drawing;
using System.Configuration;
using System.Collections;
using System.Xml;

using BattleNET;

using System.Windows.Interop;

using System.Windows.Media.Animation;

//using System.Runtime.InteropServices;

using MahApps.Metro.Controls;

using Hardcodet.Wpf.TaskbarNotification;

namespace Arma_3_Launcher_by_HC
{
    /// <summary>
    /// Логика взаимодействия для GeneralWindow.xaml
    /// </summary>
    public partial class GeneralWindow : Window
    {
        //175,10,0,0

        //Height="462" Width="840"

        public static string WhatModDownload = string.Empty;
        public static string[] stDown = null;
        public static List<string> folders = new List<string>();
        private string[] stHash = null;
        private string WhatServerPlay = string.Empty;
        private bool LaunchCheckLauncher = false;
        private DownloadWindow DownloadWindow = null;
        private bool UpdateMod = false;
        private bool FastUpdateMod = false;
        private Thread Check = null;
        private int ProgressRingIsActiveCount = 0;

        private string FTP_IP = "cramerstation.ru";
        private string FTP_USER = "launcher";
        private string FTP_PASSWORD = "flAjzm1eCj";

        //62.109.13.179
        //launcher
        //flAjzm1eCj

        private List<string> ServerStatusOn = new List<string>();

        Process proc = new Process();
        //private string TXTFile = "";
        //private bool DownloadIsActiveWindow = false;

        //private List<string> NameOfThread = new List<string>();
        //private List<Thread> Threads = new List<Thread>();

        //private IList[] AdditionalThread = new IList[2];
        //private List<AdditionalThreads> AdditionalThread = new List<AdditionalThreads>();


        public GeneralWindow()
        {
            InitializeComponent();
        }

        public class ExpanderListViewModel
        {
            public Object SelectedExpander { get; set; }
        }

        //Один ли лаунчер запущен из одной папки
        public static bool IsTheOnlyInstance()
        {
            Process pThis = Process.GetCurrentProcess();

            Process[] ps = Process.GetProcessesByName(pThis.ProcessName);

            bool flag = false;

            foreach (Process p in ps)
            {
                if (p.StartInfo.FileName == pThis.StartInfo.FileName)
                {
                    if (flag)

                        return false;

                    else

                        flag = true;
                }
                    
            }
            return true;
        }

        #region Функции трея
        //Функции трея///////////////////////////////////////////////////
        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);
            HwndSource source = PresentationSource.FromVisual(this) as HwndSource;
            source.AddHook(WndProc);
        }

        private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            if (msg == NativeMethods.WM_SHOWME)
            {
                if (DownloadWindow == null)
                {
                    if (this.IsVisible != true)
                    {
                        Visibility = Visibility.Visible;
                        this.Topmost = true;
                        this.Focus();
                        this.Topmost = false;
                        //Visibility = Visibility.Visible;
                    }
                    else
                    {
                        this.WindowState = System.Windows.WindowState.Normal;
                        this.Topmost = true;
                        this.Focus();
                        this.Topmost = false;
                        //this.WindowState = System.Windows.WindowState.Normal;
                    }
                }

                if (DownloadWindow != null)
                {
                    if (DownloadWindow.IsVisible != true)
                    {
                        DownloadWindow.Visibility = Visibility.Visible;
                        DownloadWindow.Focus();
                    }
                    else
                    {
                        DownloadWindow.WindowState = System.Windows.WindowState.Normal;
                        DownloadWindow.Focus();
                    }
                }
                //this.ShowDialog();
            }
            return IntPtr.Zero;
        }
        /////////////////////////////////////////////////////////////////
        #endregion

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            if (IsTheOnlyInstance() == false)
            {
                Tray.Visibility = Visibility.Hidden;
                System.Windows.MessageBox.Show("Уже один лаунчер запущен!", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);

                Tray.Dispose();

                ThreadStart ts = delegate()
                {
                    Dispatcher.BeginInvoke((Action)delegate()
                    {
                        Application.Current.Shutdown();
                    });
                };
                Thread t = new Thread(ts);
                t.Start();
            }

            Random rnd = new Random();
            switch (rnd.Next(1, 9))
            {
                case 1:
                    this.Background = new ImageBrush(new BitmapImage(new Uri(@"pack://application:,,,/Arma 3 Launcher by HC;component/Resources/general_backgrounds/background_picture_1.png")));
                    break;
                case 2:
                    this.Background = new ImageBrush(new BitmapImage(new Uri(@"pack://application:,,,/Arma 3 Launcher by HC;component/Resources/general_backgrounds/background_picture_2.png")));
                    break;
                case 3:
                    this.Background = new ImageBrush(new BitmapImage(new Uri(@"pack://application:,,,/Arma 3 Launcher by HC;component/Resources/general_backgrounds/background_picture_3.png")));
                    break;
                case 4:
                    this.Background = new ImageBrush(new BitmapImage(new Uri(@"pack://application:,,,/Arma 3 Launcher by HC;component/Resources/general_backgrounds/background_picture_4.png")));
                    break;
                case 5:
                    this.Background = new ImageBrush(new BitmapImage(new Uri(@"pack://application:,,,/Arma 3 Launcher by HC;component/Resources/general_backgrounds/background_picture_5.png")));
                    break;
                case 6:
                    this.Background = new ImageBrush(new BitmapImage(new Uri(@"pack://application:,,,/Arma 3 Launcher by HC;component/Resources/general_backgrounds/background_picture_6.png")));
                    break;
                case 7:
                    this.Background = new ImageBrush(new BitmapImage(new Uri(@"pack://application:,,,/Arma 3 Launcher by HC;component/Resources/general_backgrounds/background_picture_7.png")));
                    break;
                case 8:
                    this.Background = new ImageBrush(new BitmapImage(new Uri(@"pack://application:,,,/Arma 3 Launcher by HC;component/Resources/general_backgrounds/background_picture_8.png")));
                    break;
                default:
                    this.Background = new ImageBrush(new BitmapImage(new Uri(@"pack://application:,,,/Arma 3 Launcher by HC;component/Resources/general_backgrounds/background_picture_1.png")));
                    break;
            }

            this.Topmost = true;
            this.Focus();
            this.Topmost = false;

            if (File.Exists(Environment.CurrentDirectory + "\\Auto updater of Launcher.exe"))
                File.Delete(Environment.CurrentDirectory + "\\Auto updater of Launcher.exe");

            if (Properties.Settings.Default.ArmaPath == "" || !(File.Exists(Properties.Settings.Default.ArmaPath)))
            {
                this.Hide();
                FirstChooseArmaPath Form = new FirstChooseArmaPath();
                Tray.Visibility = Visibility.Hidden;
                Form.ShowDialog();
                Tray.Visibility = Visibility.Visible;
                this.Show();
            }

            

            //for (int i = 0; i < ListBoxServer.Items.Count; i++)
            //{
            //    string tmp = (ListBoxServer.Items[i] as ListBoxItem).Name.Replace("ListBoxItem", string.Empty);

            //    if (tmp == "arma_3")
            //        continue;

            //    //https://heavy-company.ru/img/logo/exile_logo.jpg
            //    Thread LoadServersLogoThread = new Thread(() => imageFromUrl("https://" + "heavy-company.ru" + "/img/logo/" + tmp + ".jpg", tmp));
            //    LoadServersLogoThread.IsBackground = true;
            //    LoadServersLogoThread.SetApartmentState(ApartmentState.STA);
            //    LoadServersLogoThread.Start();
            //}

            //if (!(File.Exists(Properties.Settings.Default.ArmaPath)))
            //{
            //    this.Hide();
            //    FirstChooseArmaPath Form = new FirstChooseArmaPath();
            //    Tray.Visibility = Visibility.Hidden;
            //    Form.ShowDialog();
            //    Tray.Visibility = Visibility.Visible;
            //    this.Show();
            //}

            Current_Path_To_Arma.Text = Properties.Settings.Default.ArmaPath;

            ModsStackPanel.DataContext = new ExpanderListViewModel();

            //OnCloseGoInTray
            if (Properties.Settings.Default.OnCloseGoInTray == true)
            {
                OnCloseGoInTray.IsChecked = true;
            }
            else
            {
                OnCloseGoInTray.IsChecked = false;
            }
            //OnMinimizeGoInTray
            if (Properties.Settings.Default.OnMinimizeGoInTray == true)
            {
                OnMinimizeGoInTray.IsChecked = true;
            }
            else
            {
                OnMinimizeGoInTray.IsChecked = false;
            }

            //AdditionalThread[0] = new List<String>();
            //AdditionalThread[1] = new List<Thread>();

            //NameOfThread.Clear();
            folders.Clear();
            //Threads.Clear();

            VersionOfLauncher.Text = " Версия: " + Convert.ToString(Assembly.GetExecutingAssembly().GetName().Version);

            LaunchCheckLauncher = true;

            //if (NameOfThread.Find(x => x == "CheckLauncherVersion") == null)
            //{
            //    NameOfThread.Add("CheckLauncherVersion");
            //    Threads.Add(new Thread(new ThreadStart(CheckLauncherVersion)));
            //    Threads.Last().Start();
            //}
            //else
            //    Threads[NameOfThread.FindIndex(x => x == "CheckLauncherVersion")].Start();

            //Threads.Add(new Thread(new ThreadStart(CheckLauncherVersion)));
            //Threads.Last().Start();

            Thread CheckLauncherVersionThread = new Thread(new ThreadStart(CheckLauncherVersion));
            CheckLauncherVersionThread.IsBackground = true;
            CheckLauncherVersionThread.Start();
        }
        
        //Проверить версию лаунчера
        private void CheckLauncherVersion()
        {
            Dispatcher.Invoke((Action)(() =>
            {
                CheckLauncher.IsEnabled = false;
            }));

            if (File.Exists(Environment.CurrentDirectory + "\\Auto updater of Launcher.exe"))
                File.Delete(Environment.CurrentDirectory + "\\Auto updater of Launcher.exe");

            string[] stHashCheckLauncher = getTXT_ASCII_File("launcher/Hash.txt").Split(new Char[] { '\t', '\n' });
            
            try
            {
                if (ComputeMD5Checksum(Assembly.GetExecutingAssembly().Location) != stHashCheckLauncher[0] && stHashCheckLauncher[0] != string.Empty)
                {
                    MessageBoxResult result = System.Windows.MessageBox.Show("Есть более новая версия лаунчера. Обновить сейчас?", "Обновление лаунчера", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (result == MessageBoxResult.Yes)
                    {
                        try
                        {
                            try
                            {
                                //FtpWebRequest rew = (FtpWebRequest)WebRequest.Create("ftp://" + FTP_IP + "/launcher/Auto updater of Launcher.exe");

                                //string host = null;
                                //string user = null;
                                //string pass = null;
                                //FtpWebRequest ftpRequest = null;
                                //FtpWebResponse ftpResponse = null;
                                //Stream ftpStream = null;
                                //int bufferSize = 2048;

                                Dispatcher.BeginInvoke((Action)delegate()
                                {
                                    this.Visibility = Visibility.Hidden;
                                    Tray.Dispose();
                                });

                                /* Create an FTP Request */
                                FtpWebRequest ftpRequest = (FtpWebRequest)FtpWebRequest.Create("ftp://" + FTP_IP + "/launcher/Auto updater of Launcher.exe");
                                ftpRequest.Timeout = 600000;
                                /* Log in to the FTP Server with the User Name and Password Provided */
                                ftpRequest.Credentials = new NetworkCredential(FTP_USER, FTP_PASSWORD);
                                /* When in doubt, use these options */
                                ftpRequest.KeepAlive = false;
                                /* Specify the Type of FTP Request */
                                ftpRequest.Method = WebRequestMethods.Ftp.DownloadFile;
                                /* Establish Return Communication with the FTP Server */
                                FtpWebResponse ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
                                /* Get the FTP Server's Response Stream */
                                Stream ftpStream = ftpResponse.GetResponseStream();
                                /* Open a File Stream to Write the Downloaded File */
                                FileStream localFileStream = new FileStream(Environment.CurrentDirectory + "\\Auto updater of Launcher.exe", FileMode.Create);
                                /* Buffer for the Downloaded Data */
                                byte[] byteBuffer = new byte[2048];
                                int bytesRead = ftpStream.Read(byteBuffer, 0, 2048);
                                /* Download the File by Writing the Buffered Data Until the Transfer is Complete */
                                try
                                {
                                    while (bytesRead > 0)
                                    {
                                        localFileStream.Write(byteBuffer, 0, bytesRead);
                                        bytesRead = ftpStream.Read(byteBuffer, 0, 2048);
                                    }
                                }
                                catch (ThreadAbortException)
                                {
                                    throw;
                                }
                                catch (Exception ex)
                                {
                                    System.Windows.MessageBox.Show("Ошибка: " + ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);

                                }

                                /* Resource Cleanup */
                                localFileStream.Close();
                                ftpStream.Close();
                                ftpResponse.Close();
                                ftpRequest = null;
                            }
                            catch (ThreadAbortException)
                            {
                                throw;
                            }
                            catch (Exception ex)
                            {
                                System.Windows.MessageBox.Show("Ошибка: " + ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                            }

                            proc.StartInfo.UseShellExecute = true;
                            proc.StartInfo.FileName = "Auto updater of Launcher.exe";
                            proc.Start();

                            ThreadStart ts = delegate()
                            {
                                Dispatcher.BeginInvoke((Action)delegate()
                                {
                                    Application.Current.Shutdown();
                                });
                            };
                            Thread t = new Thread(ts);
                            t.Start();
                        }
                        catch (ThreadAbortException)
                        {
                            throw;
                        }
                        catch (Exception ex)
                        {
                            System.Windows.MessageBox.Show("Ошибка: " + ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);

                            ThreadStart ts = delegate()
                            {
                                Dispatcher.BeginInvoke((Action)delegate()
                                {
                                    Application.Current.Shutdown();
                                });
                            };
                            Thread t = new Thread(ts);
                            t.Start();
                        }
                    }
                    else
                    {
                        Dispatcher.Invoke((Action)(() =>
                        {
                            CheckLauncher.IsEnabled = true;
                        }));
                        LaunchCheckLauncher = false;
                    }
                }
                else if (LaunchCheckLauncher == true && stHashCheckLauncher[0] != string.Empty)
                {
                    Dispatcher.Invoke((Action)(() =>
                    {
                        CheckLauncher.IsEnabled = true;
                    }));
                    LaunchCheckLauncher = false;
                }
                else if (stHashCheckLauncher[0] != string.Empty)
                {
                    Dispatcher.Invoke((Action)(() =>
                    {
                        CheckLauncher.IsEnabled = true;
                    }));

                    System.Windows.MessageBox.Show("У вас последняя версия лаунчера", "Обновление лаунчера", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                stHashCheckLauncher = null;




                //if (Convert.ToInt16(Convert.ToString(Assembly.GetExecutingAssembly().GetName().Version).Replace(".", string.Empty)) < Convert.ToDouble(stHashCheckLauncher[0].Replace(".", string.Empty)))
                //{
                //    MessageBoxResult result = System.Windows.MessageBox.Show("Есть более новая версия лаунчера. Обновить сейчас?", "", MessageBoxButton.YesNo);
                //    if (result == MessageBoxResult.Yes)
                //    {
                //        try
                //        {
                //            proc.StartInfo.UseShellExecute = true;
                //            proc.StartInfo.FileName = "Auto updater of Launcher.exe";
                //            proc.Start();

                //            System.Diagnostics.Process.GetCurrentProcess().Kill();
                //        }
                //        catch (ThreadAbortException)
                //        {
                //            throw;
                //        }
                //        catch (Exception ex)
                //        {
                //            System.Windows.MessageBox.Show("Ошибка: " + ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                //        }
                //    }
                //    else
                //    {
                //        Dispatcher.Invoke((Action)(() =>
                //        {
                //            CheckLauncher.IsEnabled = true;
                //        }));
                //        LaunchCheckLauncher = false;
                //    }
                //}
                //else if (LaunchCheckLauncher == true)
                //{
                //    Dispatcher.Invoke((Action)(() =>
                //    {
                //        CheckLauncher.IsEnabled = true;
                //    }));
                //    LaunchCheckLauncher = false;
                //}
                //else
                //{
                //    Dispatcher.Invoke((Action)(() =>
                //    {
                //        CheckLauncher.IsEnabled = true;
                //    }));

                //    System.Windows.MessageBox.Show("У вас последняя версия лаунчера");
                //}
                //stHashCheckLauncher = null;
            }
            catch (ThreadAbortException)
            {
                throw;
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Ошибка: " + ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        #region Функции работы с интернетом
        //Функции работы с ФТП///////////////////////////////////////////
        private string getTXT_UTF8_File(string PathToFile)
        {
            Dispatcher.Invoke((Action)(() =>
            {
                ProgressRingIsActiveCount++;
                ProgressRing.IsActive = true;
            }));

            // Объект запроса
            FtpWebRequest rew = (FtpWebRequest)WebRequest.Create("ftp://" + FTP_IP + "/" + PathToFile);
            //600000
            rew.Timeout = 600000;
            rew.Credentials = new NetworkCredential(FTP_USER, FTP_PASSWORD);
            rew.ServicePoint.Expect100Continue = false;
            rew.KeepAlive = false;
            rew.UseBinary = false;
            rew.EnableSsl = false;

            // Отправить запрос и получить ответ
            //FtpWebResponse resp = null;

            // Получить поток
            Stream str = new MemoryStream();

            try
            {
                FtpWebResponse resp = (FtpWebResponse)rew.GetResponse();
                str = resp.GetResponseStream();

                List<byte> newFileData = new List<byte>();

                for (int ch; ;)
                {
                    ch = str.ReadByte();
                    if (ch == -1) break;
                    newFileData.Add(Convert.ToByte(ch));
                    //test[i - 1] = ch;
                }

                str.Close();
                resp.Close();

                Dispatcher.Invoke((Action)(() =>
                {
                    ProgressRingIsActiveCount--;
                    if (ProgressRingIsActiveCount == 0)
                        ProgressRing.IsActive = false;
                }));

                return System.Text.Encoding.UTF8.GetString(newFileData.ToArray());

            }
            catch (ThreadAbortException)
            {
                str.Close();
                //resp.Close();

                Dispatcher.Invoke((Action)(() =>
                {
                    ProgressRingIsActiveCount--;
                    if (ProgressRingIsActiveCount == 0)
                        ProgressRing.IsActive = false;
                }));

                return string.Empty;
            }
            catch (Exception ex)
            {
                str.Close();
                //resp.Close();

                Dispatcher.Invoke((Action)(() =>
                {
                    ProgressRingIsActiveCount--;
                    if (ProgressRingIsActiveCount == 0)
                        ProgressRing.IsActive = false;
                }));

                System.Windows.MessageBox.Show("Ошибка: " + ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return string.Empty;
            }
        }

        private string getTXT_ASCII_File(string PathToFile)
        {
            Dispatcher.Invoke((Action)(() =>
            {
                ProgressRingIsActiveCount++;
                ProgressRing.IsActive = true;
            }));

            // Объект запроса
            FtpWebRequest rew = (FtpWebRequest)WebRequest.Create("ftp://" + FTP_IP + "/" + PathToFile);
            //600000
            rew.Timeout = 600000;
            rew.Credentials = new NetworkCredential(FTP_USER, FTP_PASSWORD);
            rew.ServicePoint.Expect100Continue = false;
            rew.KeepAlive = false;
            rew.UseBinary = false;
            rew.EnableSsl = false;

            // Отправить запрос и получить ответ
            //FtpWebResponse resp = null;

            Stream str = new MemoryStream();

            try
            {
                FtpWebResponse resp = (FtpWebResponse)rew.GetResponse();
                str = resp.GetResponseStream();

                string message = string.Empty;

                for (int ch; ; )
                {
                    ch = str.ReadByte();
                    if (ch == -1) break;
                    message += (char)ch;
                }

                str.Close();
                resp.Close();

                Dispatcher.Invoke((Action)(() =>
                {
                    ProgressRingIsActiveCount--;
                    if (ProgressRingIsActiveCount == 0)
                        ProgressRing.IsActive = false;
                }));

                string[] tmp = message.Split(new char[]{ '\0', '\a', '\b', '\v', '\f', '\r' });

                return message = string.Join(string.Empty, tmp);
            }
            catch (ThreadAbortException)
            {
                str.Close();
                //resp.Close();

                Dispatcher.Invoke((Action)(() =>
                {
                    ProgressRingIsActiveCount--;
                    if (ProgressRingIsActiveCount == 0)
                        ProgressRing.IsActive = false;
                }));

                return string.Empty;
            }
            catch (Exception ex)
            {
                str.Close();
                //resp.Close();

                Dispatcher.Invoke((Action)(() =>
                {
                    ProgressRingIsActiveCount--;
                    if (ProgressRingIsActiveCount == 0)
                        ProgressRing.IsActive = false;
                }));

                System.Windows.MessageBox.Show("Ошибка: " + ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return string.Empty;
            }
        }

        private List<string> getFTP_ListDirectory(string PathToFolder)
        {
            Dispatcher.Invoke((Action)(() =>
            {
                ProgressRingIsActiveCount++;
                ProgressRing.IsActive = true;
            }));

            List<string> ListOfFolders = new List<string>();

            // Объект запроса
            FtpWebRequest rew = (FtpWebRequest)WebRequest.Create("ftp://" + FTP_IP + "/" + PathToFolder);
            rew.Timeout = 600000;
            rew.Credentials = new NetworkCredential(FTP_USER, FTP_PASSWORD);
            rew.Method = WebRequestMethods.Ftp.ListDirectory;
            rew.KeepAlive = false;

            // Отправить запрос и получить ответ
            //FtpWebResponse resp = null;

            Stream str = new MemoryStream();
            StreamReader reader = null;

            try
            {

                FtpWebResponse resp = (FtpWebResponse)rew.GetResponse();

                str = resp.GetResponseStream();
                reader = new StreamReader(str);

                while (!reader.EndOfStream)
                {
                    DispatcherHelper.DoEvents();
                    string test = reader.ReadLine();
                    string test2 = System.IO.Path.GetExtension(test);
                    if (test.IndexOf('.') > 1)
                        continue;
                    if (test2 == string.Empty)
                        ListOfFolders.Add(test);
                }

                ListOfFolders.Sort();

                if (reader != null)
                    reader.Close();
                str.Close();
                resp.Close();

                Dispatcher.Invoke((Action)(() =>
                {
                    ProgressRingIsActiveCount--;
                    if (ProgressRingIsActiveCount == 0)
                        ProgressRing.IsActive = false;
                }));

                return ListOfFolders;
            }
            catch (ThreadAbortException)
            {
                if(reader != null)
                    reader.Close();
                str.Close();
                //resp.Close();

                Dispatcher.Invoke((Action)(() =>
                {
                    ProgressRingIsActiveCount--;
                    if (ProgressRingIsActiveCount == 0)
                        ProgressRing.IsActive = false;
                }));

                ListOfFolders.Clear();

                return ListOfFolders;
            }
            catch (Exception ex)
            {
                if (reader != null)
                    reader.Close();
                str.Close();
                //resp.Close();

                Dispatcher.Invoke((Action)(() =>
                {
                    ProgressRingIsActiveCount--;
                    if (ProgressRingIsActiveCount == 0)
                        ProgressRing.IsActive = false;
                }));

                ListOfFolders.Clear();

                System.Windows.MessageBox.Show("Ошибка: " + ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);

                return ListOfFolders;
            }
        }

        private string getHTTP(string url)
        {
            Dispatcher.Invoke((Action)(() =>
            {
                ProgressRingIsActiveCount++;
                ProgressRing.IsActive = true;
            }));

            // Объект запроса
            WebRequest req = WebRequest.Create(url);

            // Отправить запрос и получить ответ
            //FtpWebResponse resp = null;

            Stream str = new MemoryStream();
            StreamReader reader = null;

            try
            {
                WebResponse resp = req.GetResponse();
                reader = new StreamReader(resp.GetResponseStream());

                Dispatcher.Invoke((Action)(() =>
                {
                    ProgressRingIsActiveCount--;
                    if (ProgressRingIsActiveCount == 0)
                        ProgressRing.IsActive = false;
                }));

                return reader.ReadToEnd().Trim();
            }
            catch (ThreadAbortException)
            {
                if (reader != null)
                    reader.Close();
                str.Close();
                //resp.Close();

                Dispatcher.Invoke((Action)(() =>
                {
                    ProgressRingIsActiveCount--;
                    if (ProgressRingIsActiveCount == 0)
                        ProgressRing.IsActive = false;
                }));

                return reader.ReadToEnd().Trim();
            }
            catch (Exception ex)
            {
                if (reader != null)
                    reader.Close();
                str.Close();
                //resp.Close();

                Dispatcher.Invoke((Action)(() =>
                {
                    ProgressRingIsActiveCount--;
                    if (ProgressRingIsActiveCount == 0)
                        ProgressRing.IsActive = false;
                }));

                System.Windows.MessageBox.Show("Ошибка: " + ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);

                return reader.ReadToEnd().Trim();
            }
        }

        private void imageFromUrl(string url, string server)
        {
            Dispatcher.Invoke((Action)(() =>
            {
                ProgressRingIsActiveCount++;
                ProgressRing.IsActive = true;
            }));

            BitmapImage bitmap = new BitmapImage();

            System.Windows.Controls.Image test = new System.Windows.Controls.Image();

            try
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    test = (this.FindName(server + "Image") as System.Windows.Controls.Image);
                }));

                bitmap.BeginInit();
                bitmap.UriSource = new Uri(@url, UriKind.Absolute);
                bitmap.EndInit();

                Dispatcher.Invoke((Action)(() =>
                {

                    ProgressRingIsActiveCount--;
                    if (ProgressRingIsActiveCount == 0)
                        ProgressRing.IsActive = false;

                }));
            }
            catch (ThreadAbortException)
            {

                Dispatcher.Invoke((Action)(() =>
                {
                    ProgressRingIsActiveCount--;
                    if (ProgressRingIsActiveCount == 0)
                        ProgressRing.IsActive = false;
                }));
            }
            catch (Exception ex)
            {

                Dispatcher.Invoke((Action)(() =>
                {
                    ProgressRingIsActiveCount--;
                    if (ProgressRingIsActiveCount == 0)
                        ProgressRing.IsActive = false;
                }));

                System.Windows.MessageBox.Show("Ошибка: " + ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        /////////////////////////////////////////////////////////////////
        #endregion

        //MD5
        private string ComputeMD5Checksum(string path)
        {
            using (var vMD5 = MD5.Create())
            {
                using (var stream = File.OpenRead(path))
                {
                    return BitConverter.ToString(vMD5.ComputeHash(stream)).Replace("-", string.Empty);
                }
            }
        }

        //Узнать вес папки
        private static long GetDirectorySize(string folderPath)
        {
            DirectoryInfo di = new DirectoryInfo(folderPath);
            return di.EnumerateFiles("*", SearchOption.AllDirectories).Sum(fi => fi.Length);
        }

        //Сравнение хеш-сумм локальних\серверных
        private bool Comprasion_Hash_Sum(string Clear_Path, bool Delete_Extra_Files)
        {
            if (Process.GetProcessesByName("arma3").Any() == true)
            {
                MessageBoxResult result = System.Windows.MessageBox.Show("У вас запущена игра, для стабильной проверки требуется её закрыть. Закрыть её?", "Запущена игра", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    try
                    {
                        Process[] processes = Process.GetProcessesByName("arma3");
                        processes[0].CloseMainWindow();

                        Dispatcher.Invoke((Action)(() =>
                        {
                            ProgressRingIsActiveCount++;
                            ProgressRing.IsActive = true;
                        }));

                        processes[0].Kill();

                        Dispatcher.Invoke((Action)(() =>
                        {
                            ProgressRingIsActiveCount--;
                            if (ProgressRingIsActiveCount == 0)
                                ProgressRing.IsActive = false;
                        }));
                    }
                    catch (ThreadAbortException)
                    {
                        throw;
                    }
                    catch (Exception ex)
                    {
                        System.Windows.MessageBox.Show("Ошибка: " + ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }


            folders.Clear();

            stHash = null;

            stHash = getTXT_ASCII_File("mods/" + WhatModDownload + "/Hash.txt").Split(new Char[] { '\t', '\n' });

            if (stHash[0] == string.Empty)
                return false;

            //for (int i = 0; i < stHash.Length; i++ )
            //{
            //    stHash[i] = stHash[i].Replace("\r", string.Empty);
            //}

            if (WhatModDownload == "arma_3")
            {
                string[] path = (Properties.Settings.Default.ArmaPath).Split(new Char[] { '\\' });
                var regex = new System.Text.RegularExpressions.Regex(System.Text.RegularExpressions.Regex.Escape(path[path.Length - 2] + "\\"));
                Clear_Path = regex.Replace(Properties.Settings.Default.ArmaPath.Replace("arma3.exe", ""), "", 1);
                folders.Add(path[path.Length - 2]);
            }
            else
                folders = getFTP_ListDirectory("mods/" + WhatModDownload + "/Mod/");

            try
            {
                ProgressBar pb = null;
                TextBlock tb = null;

                string stPath = string.Empty;
                string[] stFiles = null;
                string[] stFilesFull = null;
                double NumberOfTotalFiles = 0.0;
                int c = 0;
                int g = 0;
                stDown = null;
                stDown = new String[(stHash.Length + 1) / 2];

                for (int d = 0; d < folders.Count; d++)
                {
                    if (!(Directory.Exists(Clear_Path + folders[d])))
                        Directory.CreateDirectory(Clear_Path + folders[d]);
                }

                if (Delete_Extra_Files == true)
                {
                    if (WhatModDownload == "arma_3")
                    {
                        for (int f = 0; f < folders.Count; f++)
                        {
                            stPath = Clear_Path + folders[f];
                            List<string> tmp_folders = getFTP_ListDirectory("mods/" + WhatModDownload + "/Mod/arma_3/");
                            List<string> tmp_stFiles = new List<string>();
                            tmp_folders.Remove("Keys");

                            int h = 0;

                            if (!File.Exists(stPath + "\\" + tmp_folders[h]))
                                continue;

                            for (; h < tmp_folders.Count; h++)
                            {
                                stFiles = Directory.GetFiles(stPath + "\\" + tmp_folders[h], "*.*", SearchOption.AllDirectories);
                                tmp_stFiles.AddRange(stFiles);
                            }


                            stFilesFull = new String[tmp_stFiles.Count];
                            (tmp_stFiles.ToArray()).CopyTo(stFilesFull, 0);

                            for (int i = 0; i < tmp_stFiles.Count; i++)
                            {
                                tmp_stFiles[i] = tmp_stFiles[i].Remove(0, stPath.Length);
                            }

                            for (int i = 0; tmp_stFiles.Count > i; i++)
                            {
                                int t = Array.IndexOf(stHash, tmp_stFiles[i]);
                                if (t == -1)
                                {
                                    File.Delete(stFilesFull[i]);
                                }
                            }

                            tmp_folders.Clear();
                            tmp_stFiles.Clear();
                        }
                    }
                    else
                    {
                        for (int f = 0; f < folders.Count; f++)
                        {
                            stPath = Clear_Path + folders[f];
                            stFiles = Directory.GetFiles(stPath, "*.*", SearchOption.AllDirectories);
                            stFilesFull = new String[stFiles.Length];
                            stFiles.CopyTo(stFilesFull, 0);

                            for (int i = 0; i < stFiles.Length; i++)
                            {
                                stFiles[i] = stFiles[i].Remove(0, stPath.Length);
                            }

                            for (int i = 0; stFiles.Length > i; i++)
                            {
                                int t = Array.IndexOf(stHash, stFiles[i]);
                                if (t == -1)
                                {
                                    File.Delete(stFilesFull[i]);
                                }
                            }
                        }
                    }
                }

                for (int d = 0; d < folders.Count; d++)
                {
                    NumberOfTotalFiles += (Directory.GetFiles((Clear_Path + folders[d]), "*.*", SearchOption.AllDirectories)).Length;
                }


                Dispatcher.Invoke((Action)(() =>
                {
                    pb = this.FindName(WhatModDownload + "ModProgressBar") as ProgressBar;
                    pb.Maximum = NumberOfTotalFiles;
                    pb.Value = 0.0;

                    (this.FindName(WhatModDownload + "ModProgressBarPercent") as TextBlock).Text = Math.Round(((pb.Value / NumberOfTotalFiles) * 100), 0) + "%";
                    //tb.Text = Math.Round(((pb.Value / NumberOfTotalFiles) * 100), 0) + "%";

                    (this.FindName(WhatModDownload + "ModNumberOfFiles") as TextBlock).Text = "Проверено 0 из " + NumberOfTotalFiles;
                    //tb.Text = "Проверено 0 из " + NumberOfTotalFiles;
                    //EpochProgressBar.Maximum = stFiles.Length;
                }));

                for (int f = 0; f < folders.Count; f++)
                {
                    stPath = Clear_Path + folders[f];
                    stFiles = Directory.GetFiles(stPath, "*.*", SearchOption.AllDirectories);
                    stFilesFull = new String[stFiles.Length];
                    stFiles.CopyTo(stFilesFull, 0);

                    for (int i = 0; i < stFiles.Length; i++)
                    {
                        stFiles[i] = stFiles[i].Remove(0, stPath.Length);
                    }

                    for (int i = g; stHash[i] != ""; i = i + 2)
                    {
                        int t = Array.IndexOf(stFiles, stHash[i]);
                        if (stHash[i].Length != 0)
                        {
                            if (t != -1)
                            {
                                if (ComputeMD5Checksum(stFilesFull[t]) != stHash[i + 1])
                                {
                                    stDown[c] = folders[f] + stHash[i];
                                    c++;
                                }
                            }
                            else
                            {
                                stDown[c] = folders[f] + stHash[i];
                                c++;
                            }
                        }
                        g = i;

                        Dispatcher.Invoke((Action)(() =>
                        {
                            pb.Value++;

                            (this.FindName(WhatModDownload + "ModProgressBarPercent") as TextBlock).Text = Math.Round(((pb.Value / NumberOfTotalFiles) * 100), 0) + "%"; ;
                            //tb.Text = Math.Round(((pb.Value / NumberOfTotalFiles) * 100), 0) + "%";

                            (this.FindName(WhatModDownload + "ModNumberOfFiles") as TextBlock).Text = "Проверено " + pb.Value + " из " + NumberOfTotalFiles;
                            //tb.Text = "Проверено " + pb.Value + " из " + NumberOfTotalFiles;
                        }));
                    }
                    g += 3;

                    Dispatcher.Invoke((Action)(() =>
                    {
                        pb.Value++;

                        (this.FindName(WhatModDownload + "ModProgressBarPercent") as TextBlock).Text = Math.Round(((pb.Value / NumberOfTotalFiles) * 100), 0) + "%";
                        //tb.Text = Math.Round(((pb.Value / NumberOfTotalFiles) * 100), 0) + "%";

                        (this.FindName(WhatModDownload + "ModNumberOfFiles") as TextBlock).Text = "Проверено " + pb.Value + " из " + NumberOfTotalFiles;
                        //tb.Text = "Проверено " + pb.Value + " из " + NumberOfTotalFiles;
                    }));
                }

                Dispatcher.Invoke((Action)(() =>
                {

                    tb = this.FindName(WhatModDownload + "ModProgressBarPercent") as TextBlock;
                    if ((pb.Value == 0.0 && NumberOfTotalFiles == 0.0) || tb.Text != "100%")
                        tb.Text = "100%";
                    //if ((tb.Text = Math.Round(((pb.Value / NumberOfTotalFiles) * 100), 0) + "%") == "NaN%" || (tb.Text = Math.Round(((pb.Value / NumberOfTotalFiles) * 100), 0) + "%") != "100%")
                    //    tb.Text = "100%";

                    if (pb.Value != NumberOfTotalFiles)
                        pb.Value = NumberOfTotalFiles;

                    tb = this.FindName(WhatModDownload + "ModNumberOfFiles") as TextBlock;
                    if (tb.Text == "Проверено 0 из 0")
                        tb.Text = "Проверено все файлы";
                    //if ((tb.Text = "Проверено " + pb.Value + " из " + NumberOfTotalFiles) == "Проверено 0 из 0")
                    //    tb.Text = "Проверено все файлы";

                }));

                stFiles = null;
                stFilesFull = null;
                g = 0;

                stHash = null;

                if (c == 0)
                    return false;
                else
                    return true;
            }
            catch (ThreadAbortException)
            {
                return false;
                throw;
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Ошибка: " + ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
        }

        //Проверить моды
        private void CheckMods()
        {
            Dispatcher.Invoke((Action)(() =>
            {
                if (OptionsSlidePanel.IsOpen == true)
                    OptionsSlidePanel.IsOpen = false;

                ChangePathArma.IsEnabled = false;
                CheckLauncher.IsEnabled = false;
                ChooseServer.IsEnabled = false;
            }));

            if(WhatModDownload == "arma_3")
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    Mods.IsEnabled = false;
                }));

                MessageBoxResult result = new MessageBoxResult();

                //AssemblyName assemblyName = AssemblyName.GetAssemblyName("C:\\Windows\\Temp\\arma3.exe");
                //string test = assemblyName.Version.ToString();
                string version_server = getTXT_ASCII_File("mods/arma_3/version.txt");

                try
                {
                    string version_local = string.Empty;
                    if (File.Exists(Properties.Settings.Default.ArmaPath))
                    {
                        var fileversioninfo = FileVersionInfo.GetVersionInfo(Properties.Settings.Default.ArmaPath);
                        version_local = fileversioninfo.FileMajorPart.ToString() + "." + fileversioninfo.FileMinorPart.ToString() + "." + fileversioninfo.FileBuildPart.ToString() + "." + fileversioninfo.FilePrivatePart.ToString();
                    }
                    else
                        version_local = "-";
                    
                    //string version = (FileVersionInfo.GetVersionInfo("C:\\Windows\\Temp\\arma3.exe")).FileMajorPart.ToString();

                    if (Comprasion_Hash_Sum(Properties.Settings.Default.ArmaPath.Replace("arma3.exe", ""), true) == false)
                    {
                        //(this.FindName(WhatModDownload + "EpochLocalVersion") as TextBlock).Text = version;
                        //(this.FindName(WhatModDownload + "EpochServerVersion") as TextBlock).Text = version;

                        Dispatcher.Invoke((Action)(() =>
                        {
                            (this.FindName(WhatModDownload + "LocalVersion") as TextBlock).Text = version_server;
                            (this.FindName(WhatModDownload + "ServerVersion") as TextBlock).Text = version_server;
                        }));

                        System.Windows.MessageBox.Show("Все файлы успешно прошли проверку\nУ вас последняя версия игры", "Нет обновления", MessageBoxButton.OK, MessageBoxImage.Information);

                        UpdateMod = false;
                    }
                    else
                    {
                        Dispatcher.Invoke((Action)(() =>
                        {
                            (this.FindName(WhatModDownload + "LocalVersion") as TextBlock).Text = version_local;
                            (this.FindName(WhatModDownload + "ServerVersion") as TextBlock).Text = version_server;
                        }));

                        if (UpdateMod != true)
                        {
                            result = System.Windows.MessageBox.Show("Есть более новая версия\nХотите загрузить?", "Есть обновление", MessageBoxButton.YesNo, MessageBoxImage.Information);
                        }

                        UpdateMod = true;
                    }
                }
                catch (ThreadAbortException)
                {
                    throw;
                }
                catch (Exception ex)
                {
                    System.Windows.MessageBox.Show("Ошибка: " + ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                }

                Dispatcher.Invoke((Action)(() =>
                {
                    ChangePathArma.IsEnabled = true;
                    CheckLauncher.IsEnabled = true;
                    ChooseServer.IsEnabled = true;
                    Mods.IsEnabled = true;

                    //LaunchOptionsGrid.Visibility = Visibility.Hidden;
                    //ChooseServerGrid.Visibility = Visibility.Hidden;
                    Update_arma.IsChecked = true;

                    this.Topmost = true;
                    this.Focus();
                    this.Topmost = false;

                    if (result == MessageBoxResult.Yes)
                    {
                        Visibility = Visibility.Hidden;

                        DownloadWindow = new DownloadWindow();
                        DownloadWindow.Closed += OnClosedDownload;
                        DownloadWindow.ShowDialog();
                    }
                }));
            }
            else
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    Update_arma.IsEnabled = false;
                }));

                MessageBoxResult result = new MessageBoxResult();

                try
                {
                    string path = Properties.Settings.Default.ArmaPath.Replace("arma3.exe", "") + "version_of_mod\\" + WhatModDownload + ".txt";
                    string version_server = (getTXT_ASCII_File("mods/" + WhatModDownload + "/version_of_mod/" + WhatModDownload + ".txt")).Replace(" ", "");
                    string version_local = string.Empty;
                    if (File.Exists(path))
                        version_local = File.ReadAllText(path).Replace(" ", "");
                    else
                        version_local = "-";

                    if (Comprasion_Hash_Sum(Properties.Settings.Default.ArmaPath.Replace("arma3.exe", ""), true) == false)
                    {
                        //(this.FindName(WhatModDownload + "EpochLocalVersion") as TextBlock).Text = version;
                        //(this.FindName(WhatModDownload + "EpochServerVersion") as TextBlock).Text = version;

                        if (!Directory.Exists(Properties.Settings.Default.ArmaPath.Replace("arma3.exe", "") + "version_of_mod"))
                        {
                            Directory.CreateDirectory(Properties.Settings.Default.ArmaPath.Replace("arma3.exe", "") + "version_of_mod");
                            //File.Create(Properties.Settings.Default.ArmaPath.Replace("arma3.exe", "") + "version_of_mod\\" + WhatModDownload + ".txt");
                            File.WriteAllText(path, version_server);
                        }
                        if (!File.Exists(path))
                        {
                            //File.Create(Properties.Settings.Default.ArmaPath.Replace("arma3.exe", "") + "version_of_mod\\" + WhatModDownload + ".txt");
                            File.WriteAllText(path, version_server);
                        }
                        if (version_server != version_local)
                        {
                            File.WriteAllText(path, String.Empty);
                            File.WriteAllText(path, version_server);
                        }

                        string test = string.Empty;

                        Dispatcher.Invoke((Action)(() =>
                        {
                            (this.FindName(WhatModDownload + "LocalVersion") as TextBlock).Text = version_server;
                            (this.FindName(WhatModDownload + "ServerVersion") as TextBlock).Text = version_server;
                        }));

                        System.Windows.MessageBox.Show("Все файлы успешно прошли проверку\nУ вас последняя версия мода", "Нет обновления", MessageBoxButton.OK, MessageBoxImage.Information);

                        UpdateMod = false;
                    }
                    else
                    {
                        if (!Directory.Exists(Properties.Settings.Default.ArmaPath.Replace("arma3.exe", "") + "version_of_mod"))
                            Directory.CreateDirectory(Properties.Settings.Default.ArmaPath.Replace("arma3.exe", "") + "version_of_mod");

                        Dispatcher.Invoke((Action)(() =>
                        {
                            if (File.Exists(path))
                                (this.FindName(WhatModDownload + "LocalVersion") as TextBlock).Text = version_local;

                            (this.FindName(WhatModDownload + "ServerVersion") as TextBlock).Text = version_server;
                        }));

                        if (UpdateMod != true)
                        {
                            result = System.Windows.MessageBox.Show("Есть более новая версия\nХотите загрузить?", "Есть обновление", MessageBoxButton.YesNo, MessageBoxImage.Information);
                        }

                        UpdateMod = true;
                    }
                }
                catch (ThreadAbortException)
                {
                    throw;
                }
                catch (Exception ex)
                {
                    System.Windows.MessageBox.Show("Ошибка: " + ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                } 

                Dispatcher.Invoke((Action)(() =>
                {
                    ChangePathArma.IsEnabled = true;
                    CheckLauncher.IsEnabled = true;
                    ChooseServer.IsEnabled = true;
                    Update_arma.IsEnabled = true;

                    //LaunchOptionsGrid.Visibility = Visibility.Hidden;
                    //ChooseServerGrid.Visibility = Visibility.Hidden;
                    Mods.IsChecked = true;

                    this.Topmost = true;
                    this.Focus();
                    this.Topmost = false;

                    if (result == MessageBoxResult.Yes)
                    {
                        Visibility = Visibility.Hidden;

                        DownloadWindow = new DownloadWindow();
                        DownloadWindow.Closed += OnClosedDownload;
                        DownloadWindow.ShowDialog();
                    }
                }));
            }
        }

        //Быстрая проверка модов
        private void FastCheckMods()
        {
            Dispatcher.Invoke((Action)(() =>
            {
                if (OptionsSlidePanel.IsOpen == true)
                    OptionsSlidePanel.IsOpen = false;

                ChangePathArma.IsEnabled = false;
                CheckLauncher.IsEnabled = false;
                ChooseServer.IsEnabled = false;
            }));

            if (WhatModDownload == "arma_3")
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    Mods.IsEnabled = false;
                }));

                string version_server = getTXT_ASCII_File("mods/arma_3/version.txt");

                string version_local = string.Empty;
                if (File.Exists(Properties.Settings.Default.ArmaPath))
                {
                    var fileversioninfo = FileVersionInfo.GetVersionInfo(Properties.Settings.Default.ArmaPath);
                    version_local = fileversioninfo.FileMajorPart.ToString() + "." + fileversioninfo.FileMinorPart.ToString() + "." + fileversioninfo.FileBuildPart.ToString() + "." + fileversioninfo.FilePrivatePart.ToString();
                }
                else
                    version_local = "-";

                List<string> ListOfFolders = getFTP_ListDirectory("mods/" + WhatModDownload + "/Mod/arma_3/");

                try
                {
                    Dispatcher.Invoke((Action)(() =>
                    {
                        (this.FindName(WhatModDownload + "ServerVersion") as TextBlock).Text = version_server;
                    }));

                    int d = 0;

                    for (int i = 0; i < ListOfFolders.Count; i++)
                    {
                        if (Directory.Exists(Properties.Settings.Default.ArmaPath.Replace("arma3.exe", "") + ListOfFolders[i]))
                            d++;
                    }

                    if (ListOfFolders.Count == d || ListOfFolders.Count > d)
                    {
                        Dispatcher.Invoke((Action)(() =>
                        {
                            (this.FindName(WhatModDownload + "LocalVersion") as TextBlock).Text = version_local;
                        }));

                        if (version_server != version_local)
                        {
                            MessageBoxResult result = System.Windows.MessageBox.Show("Есть более новая версия\nХотите сделать полную проверку сейчас?", "", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
                            if (result == MessageBoxResult.Yes)
                            {
                                UpdateMod = true;
                                CheckMods_Function();
                            }
                            else
                            {
                                FastUpdateMod = false;
                            }
                        }
                        else
                        {
                            System.Windows.MessageBox.Show("У вас последняя версия игры", "", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }
                    else
                    {
                        MessageBoxResult result = System.Windows.MessageBox.Show("Вы либо не скачали игру либо не сделали полную проверку\nХотите сделать полную проверку сейчас?", "", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
                        if (result == MessageBoxResult.Yes)
                        {
                            UpdateMod = true;
                            CheckMods_Function();
                        }
                        else
                        {
                            FastUpdateMod = false;
                        }
                    }
                }
                catch (ThreadAbortException)
                {
                    throw;
                }
                catch (Exception ex)
                {
                    System.Windows.MessageBox.Show("Ошибка: " + ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                }

                Dispatcher.Invoke((Action)(() =>
                {
                    Mods.IsEnabled = true;
                }));
            }
            else
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    Update_arma.IsEnabled = false;
                }));

                string path = Properties.Settings.Default.ArmaPath.Replace("arma3.exe", "") + "version_of_mod\\" + WhatModDownload + ".txt";
                string version_server = (getTXT_ASCII_File("mods/" + WhatModDownload + "/version_of_mod/" + WhatModDownload + ".txt")).Replace(" ", "");
                string version_local = string.Empty;
                if (File.Exists(path))
                    version_local = File.ReadAllText(path).Replace(" ", "");
                else
                    version_local = "-";
                List<string> ListOfFolders = getFTP_ListDirectory("mods/" + WhatModDownload + "/Mod/");

                try
                {
                    Dispatcher.Invoke((Action)(() =>
                    {
                        (this.FindName(WhatModDownload + "ServerVersion") as TextBlock).Text = version_server;
                    }));

                    int d = 0;

                    for (int i = 0; i < ListOfFolders.Count; i++)
                    {
                        if (Directory.Exists(Properties.Settings.Default.ArmaPath.Replace("arma3.exe", "") + ListOfFolders[i]))
                            d++;
                    }

                    if (File.Exists(path) && ListOfFolders.Count == d)
                    {
                        Dispatcher.Invoke((Action)(() =>
                        {
                            (this.FindName(WhatModDownload + "LocalVersion") as TextBlock).Text = version_local;
                        }));

                        if (version_server != version_local)
                        {
                            MessageBoxResult result = System.Windows.MessageBox.Show("Есть более новая версия\nХотите сделать полную проверку сейчас?", "", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
                            if (result == MessageBoxResult.Yes)
                            {
                                UpdateMod = true;
                                CheckMods_Function();
                            }
                            else
                            {
                                FastUpdateMod = false;
                            }
                        }
                        else
                        {
                            System.Windows.MessageBox.Show("У вас последняя версия мода", "", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }
                    else
                    {
                        MessageBoxResult result = System.Windows.MessageBox.Show("Вы либо не скачали мод либо не сделали полную проверку\nХотите сделать полную проверку сейчас?", "", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
                        if (result == MessageBoxResult.Yes)
                        {
                            UpdateMod = true;
                            CheckMods_Function();
                        }
                        else
                        {
                            FastUpdateMod = false;
                        }
                    }
                }
                catch (ThreadAbortException)
                {
                    throw;
                }
                catch (Exception ex)
                {
                    System.Windows.MessageBox.Show("Ошибка: " + ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                }

                Dispatcher.Invoke((Action)(() =>
                {
                    Update_arma.IsEnabled = true;
                }));
            }

            Dispatcher.Invoke((Action)(() =>
            {
                ChangePathArma.IsEnabled = true;
                CheckLauncher.IsEnabled = true;
                ChooseServer.IsEnabled = true;
                Update_arma.IsEnabled = true;
            }));
        }

        //Быстрая проверка модов при подключение к серверу
        private void FastCheckModsInServerGrid()
        {
            bool CheckToolTip = false;

            Dispatcher.Invoke((Action)(() =>
            {
                if (OptionsSlidePanel.IsOpen == true)
                    OptionsSlidePanel.IsOpen = false;

                ChooseServerGrid.IsEnabled = false;
                (this.FindName(WhatModDownload + "ServerDescription") as Expander).Visibility = Visibility.Hidden;
                (this.FindName(WhatModDownload + "ListOfPlayers") as Expander).Visibility = Visibility.Hidden;

                ChooseServer.IsEnabled = false;
                LaunchOptions.IsEnabled = false;
                Mods.IsEnabled = false;
                Update_arma.IsEnabled = false;
                Options.IsEnabled = false;

                //test = ((this.FindName(WhatModDownload + "ListBoxItem") as ListBoxItem).ToolTip).ToString();
                if ((this.FindName(WhatModDownload + "ListBoxItem") as ListBoxItem).ToolTip != null)
                    CheckToolTip = true;
            }));



            if (CheckToolTip == false)
            {
                string version_server = getTXT_ASCII_File("mods/arma_3/version.txt");
                string version_local = string.Empty;
                if (File.Exists(Properties.Settings.Default.ArmaPath))
                {
                    var fileversioninfo = FileVersionInfo.GetVersionInfo(Properties.Settings.Default.ArmaPath);
                    version_local = fileversioninfo.FileMajorPart.ToString() + "." + fileversioninfo.FileMinorPart.ToString() + "." + fileversioninfo.FileBuildPart.ToString() + "." + fileversioninfo.FilePrivatePart.ToString();
                }
                else
                    version_local = "-";

                List<string> ListOfFolders = getFTP_ListDirectory("mods/arma_3/Mod/arma_3/");

                try
                {
                    int d = 0;

                    for (int i = 0; i < ListOfFolders.Count; i++)
                    {
                        if (Directory.Exists(Properties.Settings.Default.ArmaPath.Replace("arma3.exe", "") + ListOfFolders[i]))
                            d++;
                    }

                    if (ListOfFolders.Count == d || ListOfFolders.Count > d)
                    {
                        if (version_server != version_local)
                        {
                            MessageBoxResult result = System.Windows.MessageBox.Show("Есть более новая версия\nХотите сделать полную проверку сейчас?", "", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
                            if (result == MessageBoxResult.Yes)
                            {

                                Dispatcher.Invoke((Action)(() =>
                                {
                                    ProgressRing.Margin = new Thickness(116, 238, 704, 162);
                                    ChooseServer.Margin = new Thickness(10, 53, 704, 375);
                                    ChooseServer.Content = "Играть";
                                    ChooseServer.IsChecked = false;

                                    LaunchOptions.Visibility = Visibility.Visible;
                                    Mods.Visibility = Visibility.Visible;
                                    Update_arma.Visibility = Visibility.Visible;
                                    Options.Visibility = Visibility.Visible;

                                    OptionsSlidePanel.IsOpen = false;

                                    Update_arma.IsChecked = true;
                                }));

                                UpdateMod = true;
                                CheckMods_Function();

                                return;
                            }
                            else
                            {
                                FastUpdateMod = false;
                            }
                        }
                    }
                    else
                    {
                        MessageBoxResult result = System.Windows.MessageBox.Show("Вы либо не скачали игру либо не сделали полную проверку\nХотите сделать полную проверку сейчас?", "", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
                        if (result == MessageBoxResult.Yes)
                        {
                            UpdateMod = true;
                            CheckMods_Function();
                            return;
                        }
                        else
                        {
                            FastUpdateMod = false;
                        }
                    }
                }
                catch (ThreadAbortException)
                {
                    throw;
                }
                catch (Exception ex)
                {
                    System.Windows.MessageBox.Show("Ошибка: " + ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                }


                version_server = (getTXT_ASCII_File("mods/" + WhatModDownload + "/version_of_mod/" + WhatModDownload + ".txt")).Replace(" ", "");
                string path = Properties.Settings.Default.ArmaPath.Replace("arma3.exe", "") + "version_of_mod\\" + WhatModDownload + ".txt";
                ListOfFolders.Clear();
                ListOfFolders = getFTP_ListDirectory("mods/" + WhatModDownload + "/Mod/");
                TextBlock tb = null;

                try
                {
                    bool StatusServer = false;
                    string TextBlock_Text = string.Empty;

                    Dispatcher.Invoke((Action)(() =>
                    {
                        tb = (this.FindName(WhatModDownload + "ServerStatus") as TextBlock);
                        TextBlock_Text = tb.Text;
                    }));

                    for (; TextBlock_Text == "-"; )
                    {
                        Dispatcher.Invoke((Action)(() =>
                        {
                            tb = (this.FindName(WhatModDownload + "ServerStatus") as TextBlock);
                            TextBlock_Text = tb.Text;
                        }));
                        Thread.Sleep(100);
                    }

                    if (TextBlock_Text == "Онлайн")
                        StatusServer = true;
                    

                    int d = 0;

                    for (int i = 0; i < ListOfFolders.Count; i++)
                    {
                        if (Directory.Exists(Properties.Settings.Default.ArmaPath.Replace("arma3.exe", "") + ListOfFolders[i]))
                            d++;
                    }

                    if (File.Exists(path) && ListOfFolders.Count == d && StatusServer == true)
                    {
                        if (version_server != (File.ReadAllText(path)).Replace(" ", ""))
                        {
                            MessageBoxResult result = System.Windows.MessageBox.Show("Есть более новая версия мода для этого сервера\nХотите сделать полную проверку?", "", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
                            if (result == MessageBoxResult.Yes)
                            {
                                Dispatcher.Invoke((Action)(() =>
                                {
                                    ProgressRing.Margin = new Thickness(116, 238, 704, 162);
                                    ChooseServer.Margin = new Thickness(10, 53, 704, 375);
                                    ChooseServer.Content = "Играть";
                                    ChooseServer.IsChecked = false;

                                    LaunchOptions.Visibility = Visibility.Visible;
                                    Mods.Visibility = Visibility.Visible;
                                    Update_arma.Visibility = Visibility.Visible;
                                    Options.Visibility = Visibility.Visible;

                                    OptionsSlidePanel.IsOpen = false;

                                    Mods.IsChecked = true;

                                    (this.FindName(WhatModDownload + "Mod") as Expander).IsExpanded = true;
                                }));

                                UpdateMod = true;
                                CheckMods_Function();
                            }
                            else
                            {
                                FastUpdateMod = false;
                            }
                        }
                        else
                        {
                            Thread Arma = new Thread(new ThreadStart(LaunchArma));
                            Arma.IsBackground = true;
                            Arma.Start();
                            Arma.Join();
                        }
                    }
                    else
                    {
                        if (TextBlock_Text == "Оффлайн")
                        {
                            System.Windows.MessageBox.Show("В данный момент сервер оффлайн", "Сервер оффланй", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                        else
                        {
                            MessageBoxResult result = System.Windows.MessageBox.Show("Вы либо не скачали мод для данного сервера либо не сделали полную проверку\nХотите сделать полную проверку?", "", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
                            if (result == MessageBoxResult.Yes)
                            {
                                Dispatcher.Invoke((Action)(() =>
                                {
                                    ProgressRing.Margin = new Thickness(116, 238, 704, 162);
                                    ChooseServer.Margin = new Thickness(10, 53, 704, 375);
                                    ChooseServer.Content = "Играть";
                                    ChooseServer.IsChecked = false;

                                    LaunchOptions.Visibility = Visibility.Visible;
                                    Mods.Visibility = Visibility.Visible;
                                    Update_arma.Visibility = Visibility.Visible;
                                    Options.Visibility = Visibility.Visible;

                                    OptionsSlidePanel.IsOpen = false;

                                    Mods.IsChecked = true;

                                    (this.FindName(WhatModDownload + "Mod") as Expander).IsExpanded = true;
                                }));

                                UpdateMod = true;
                                CheckMods_Function();
                            }
                            else
                            {
                                FastUpdateMod = false;
                            }
                        }
                    }
                }
                catch (ThreadAbortException)
                {
                    throw;
                }
                catch (Exception ex)
                {
                    System.Windows.MessageBox.Show("Ошибка: " + ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                string version = (getTXT_ASCII_File("mods/" + WhatModDownload + "/version_of_mod/" + WhatModDownload + ".txt")).Replace(" ", "");
                string path = Properties.Settings.Default.ArmaPath.Replace("arma3.exe", "") + "version_of_mod\\" + WhatModDownload + ".txt";
                List<string> ListOfFolders = getFTP_ListDirectory("mods/" + WhatModDownload + "/Mod/");
                TextBlock tb = null;

                try
                {
                    bool StatusServer = false;
                    string TextBlock_Text = string.Empty;

                    Dispatcher.Invoke((Action)(() =>
                    {
                        tb = (this.FindName(WhatModDownload + "ServerStatus") as TextBlock);
                        TextBlock_Text = tb.Text;
                    }));

                    for (; TextBlock_Text == "-"; )
                    {
                        Dispatcher.Invoke((Action)(() =>
                        {
                            tb = (this.FindName(WhatModDownload + "ServerStatus") as TextBlock);
                            TextBlock_Text = tb.Text;
                        }));
                        Thread.Sleep(100);
                    }

                    if (TextBlock_Text == "Онлайн")
                        StatusServer = true;

                    

                    int d = 0;

                    for (int i = 0; i < ListOfFolders.Count; i++)
                    {
                        if (Directory.Exists(Properties.Settings.Default.ArmaPath.Replace("arma3.exe", "") + ListOfFolders[i]))
                            d++;
                    }

                    if (File.Exists(path) && ListOfFolders.Count == d && StatusServer == true)
                    {
                        if (version != (File.ReadAllText(path)).Replace(" ", ""))
                        {
                            MessageBoxResult result = System.Windows.MessageBox.Show("Есть более новая версия мода для этого сервера\nХотите сделать полную проверку?", "", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
                            if (result == MessageBoxResult.Yes)
                            {
                                Dispatcher.Invoke((Action)(() =>
                                {
                                    ProgressRing.Margin = new Thickness(116, 238, 704, 162);
                                    ChooseServer.Margin = new Thickness(10, 53, 704, 375);
                                    ChooseServer.Content = "Играть";
                                    ChooseServer.IsChecked = false;

                                    LaunchOptions.Visibility = Visibility.Visible;
                                    Mods.Visibility = Visibility.Visible;
                                    Update_arma.Visibility = Visibility.Visible;
                                    Options.Visibility = Visibility.Visible;

                                    OptionsSlidePanel.IsOpen = false;

                                    Mods.IsChecked = true;

                                    (this.FindName(WhatModDownload + "Mod") as Expander).IsExpanded = true;
                                }));

                                UpdateMod = true;
                                CheckMods_Function();
                            }
                            else
                            {
                                FastUpdateMod = false;
                            }
                        }
                        else
                        {
                            Thread Arma = new Thread(new ThreadStart(LaunchArma));
                            Arma.IsBackground = true;
                            Arma.Start();
                            Arma.Join();
                        }
                    }
                    else
                    {
                        if (TextBlock_Text == "Оффлайн")
                        {
                            System.Windows.MessageBox.Show("В данный момент сервер оффлайн", "Сервер оффланй", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                        else
                        {
                            MessageBoxResult result = System.Windows.MessageBox.Show("Вы либо не скачали мод для данного сервера либо не сделали полную проверку\nХотите сделать полную проверку?", "", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
                            if (result == MessageBoxResult.Yes)
                            {
                                Dispatcher.Invoke((Action)(() =>
                                {
                                    ProgressRing.Margin = new Thickness(116, 238, 704, 162);
                                    ChooseServer.Margin = new Thickness(10, 53, 704, 375);
                                    ChooseServer.Content = "Играть";
                                    ChooseServer.IsChecked = false;

                                    LaunchOptions.Visibility = Visibility.Visible;
                                    Mods.Visibility = Visibility.Visible;
                                    Update_arma.Visibility = Visibility.Visible;
                                    Options.Visibility = Visibility.Visible;

                                    OptionsSlidePanel.IsOpen = false;

                                    Mods.IsChecked = true;

                                    (this.FindName(WhatModDownload + "Mod") as Expander).IsExpanded = true;
                                }));

                                UpdateMod = true;
                                CheckMods_Function();
                            }
                            else
                            {
                                FastUpdateMod = false;
                            }
                        }
                    }
                }
                catch (ThreadAbortException)
                {
                    throw;
                }
                catch (Exception ex)
                {
                    System.Windows.MessageBox.Show("Ошибка: " + ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            

            Dispatcher.Invoke((Action)(() =>
            {
                ChooseServerGrid.IsEnabled = true;
                (this.FindName(WhatModDownload + "ServerDescription") as Expander).Visibility = Visibility.Visible;
                (this.FindName(WhatModDownload + "ListOfPlayers") as Expander).Visibility = Visibility.Visible;

                ChooseServer.IsEnabled = true;
                LaunchOptions.IsEnabled = true;
                Mods.IsEnabled = true;
                Update_arma.IsEnabled = true;
                Options.IsEnabled = true;
            }));
        }

        private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }

        #region Работа с интерфейсом
        //Работа с интерфейсом///////////////////////////////////////////
        private void DragWindow_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void MinimizeButton_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (Properties.Settings.Default.OnMinimizeGoInTray == false)
                this.WindowState = System.Windows.WindowState.Minimized;
            else
                Visibility = Visibility.Hidden;
        }

        private void CloseButton_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (Properties.Settings.Default.OnCloseGoInTray == false)
            {
                if (DownloadWindow != null)
                    DownloadWindow.Close();

                Tray.Dispose();

                ThreadStart ts = delegate()
                {
                    Dispatcher.BeginInvoke((Action)delegate()
                    {
                        Application.Current.Shutdown();
                    });
                };
                Thread t = new Thread(ts);
                t.Start();
            }
            else
                Visibility = Visibility.Hidden;
        }
        /////////////////////////////////////////////////////////////////
        #endregion

        #region Работа с трейем
        //Работа с трейем////////////////////////////////////////////////
        private void CloseInTray_Click(object sender, RoutedEventArgs e)
        {
            //if(DownloadIsActiveWindow == false)
            if(DownloadWindow == null)
                if (Visibility == System.Windows.Visibility.Visible)
                    Visibility = Visibility.Hidden;
                else
                    Visibility = Visibility.Visible;
            else
                if (DownloadWindow.Visibility == System.Windows.Visibility.Visible)
                    DownloadWindow.Visibility = Visibility.Hidden;
                else
                    DownloadWindow.Visibility = Visibility.Visible;
            //DownloadWindow.Visibility = Visibility.Hidden;
            //GeneralWindow.Visibility = Visibility.Hidden;
            //Visibility = Visibility.Hidden;
        }

        private void ExitTray_Click(object sender, RoutedEventArgs e)
        {
            Tray.Dispose();

            ThreadStart ts = delegate()
            {
                Dispatcher.BeginInvoke((Action)delegate()
                {
                    Application.Current.Shutdown();
                });
            };
            Thread t = new Thread(ts);
            t.Start();
        }
        /////////////////////////////////////////////////////////////////
        #endregion

        #region Левые кнопки
        //Левые кнопки (главные)/////////////////////////////////////////
        private void LeftRadioButtonGeneral_Click(object sender, RoutedEventArgs e)
        {
            OptionsSlidePanel.IsOpen = false;

            if ((sender as RadioButton).Name == "LaunchOptions")
            {
                Thread UpdateLaunchOptionsThread = new Thread(new ThreadStart(UpdateLaunchOptions));
                UpdateLaunchOptionsThread.IsBackground = true;
                UpdateLaunchOptionsThread.Start();
            }
        }

        private void ChooseServer_Click(object sender, RoutedEventArgs e)
        {
            if (((RadioButton)sender).Margin == new Thickness(10, 395, 704, 33))
            {
                ProgressRing.Margin = new Thickness(116, 238, 704, 162);
                ((RadioButton)sender).Margin = new Thickness(10, 53, 704, 375);
                ((RadioButton)sender).Content = "Играть";
                ((RadioButton)sender).IsChecked = false;

                LaunchOptions.Visibility = Visibility.Visible;
                Mods.Visibility = Visibility.Visible;
                Update_arma.Visibility = Visibility.Visible;
                Options.Visibility = Visibility.Visible;
            }
            else
            {
                ProgressRing.Margin = new Thickness(190, 57, 630, 343);
                ((RadioButton)sender).Margin = new Thickness(10, 395, 704, 33);
                ((RadioButton)sender).Content = "Назад";
                OptionsSlidePanel.IsOpen = false;

                Thread FastServersStatusThread = new Thread(new ThreadStart(FastServersStatus));
                FastServersStatusThread.IsBackground = true;
                FastServersStatusThread.Start();

                LaunchOptions.Visibility = Visibility.Hidden;
                Mods.Visibility = Visibility.Hidden;
                Update_arma.Visibility = Visibility.Hidden;
                Options.Visibility = Visibility.Hidden;
            }
        }

        private void Options_Click(object sender, RoutedEventArgs e)
        {
            if (OptionsSlidePanel.IsOpen == false)
                OptionsSlidePanel.IsOpen = true;
            else
                OptionsSlidePanel.IsOpen = false;
        }

        private void Update_arma_Function()
        {
            string temp = WhatModDownload;
            List<string> list = getFTP_ListDirectory("mods/" + temp + "/Mod/arma_3/");
            long modsize = 0;

            for (int i = 0; i < list.Count; i++)
            {
                if (Directory.Exists(Properties.Settings.Default.ArmaPath.Replace("arma3.exe", "") + list[i]))
                    modsize += GetDirectorySize(Properties.Settings.Default.ArmaPath.Replace("arma3.exe", "") + list[i]);
            }

            Dispatcher.Invoke((Action)(() =>
            {
                if ((Math.Round(modsize / 1048576.0, 2) > 1024.0))
                {
                    if (Math.Round(modsize / 1073741824.0, 2) <= 0)
                        (this.FindName(temp + "LocalSize") as TextBlock).Text = "-";
                    else
                        (this.FindName(temp + "LocalSize") as TextBlock).Text = Math.Round(modsize / 1073741824.0, 2) + " GB";
                }
                else
                {
                    if (Math.Round(modsize / 1048576.0, 2) <= 0)
                        (this.FindName(temp + "LocalSize") as TextBlock).Text = "-";
                    else
                        (this.FindName(temp + "LocalSize") as TextBlock).Text = Math.Round(modsize / 1048576.0, 2) + " MB";
                }
            }));
        }

        private void Update_arma_Click(object sender, RoutedEventArgs e)
        {
            WhatModDownload = "arma_3";

            if (File.Exists(Properties.Settings.Default.ArmaPath))
            {
                var fileversioninfo = FileVersionInfo.GetVersionInfo(Properties.Settings.Default.ArmaPath);
                (this.FindName(WhatModDownload + "LocalVersion") as TextBlock).Text = fileversioninfo.FileMajorPart.ToString() + "." + fileversioninfo.FileMinorPart.ToString() + "." + fileversioninfo.FileBuildPart.ToString() + "." + fileversioninfo.FilePrivatePart.ToString();
            } 
            else
                (this.FindName(WhatModDownload + "LocalVersion") as TextBlock).Text = "-";

            Thread Update_armaThread = new Thread(new ThreadStart(Update_arma_Function));
            Update_armaThread.IsBackground = true;
            Update_armaThread.Start();
        }
        /////////////////////////////////////////////////////////////////
        #endregion

        #region Настройки
        //Вылетающия панель//////////////////////////////////////////////
        private void ChangePathArma_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog OPF = new Microsoft.Win32.OpenFileDialog();
            OPF.Filter = "Arma 3|arma3.exe";

            if (OPF.ShowDialog() == true)
            {
                Properties.Settings.Default.ArmaPath = OPF.FileName;
                Properties.Settings.Default.Save();

                Current_Path_To_Arma.Text = Properties.Settings.Default.ArmaPath;
            }
        }

        private void CheckLauncher_Click(object sender, RoutedEventArgs e)
        {
            Thread CheckLauncherVersionThread = new Thread(new ThreadStart(CheckLauncherVersion));
            CheckLauncherVersionThread.IsBackground = true;
            CheckLauncherVersionThread.Start();

            //Threads.Add(new Thread(new ThreadStart(CheckLauncherVersion)));
            //Threads.Last().Start();
        }

        private void AboutProgram_Click(object sender, RoutedEventArgs e)
        {
            AboutWindow window = new AboutWindow();
            window.ShowDialog();
        }

        private void Changelog_Click(object sender, RoutedEventArgs e)
        {
            ChangelogWindow window = new ChangelogWindow();
            window.ShowDialog();
        }

        private void ToggleSwitch_CheckedChanged(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default[((ToggleSwitch)sender).Name] = ((ToggleSwitch)sender).IsChecked.Value;
            Properties.Settings.Default.Save();
        }
        /////////////////////////////////////////////////////////////////
        #endregion

        #region Играть
        //Грид играть////////////////////////////////////////////////////
        private void ListOfPlayers_Expanded(object sender, RoutedEventArgs e)
        {
            FlipViewServerBrowser.HideControlButtons();
        }

        private void ListOfPlayers_Collapsed(object sender, RoutedEventArgs e)
        {
            FlipViewServerBrowser.ShowControlButtons();
        }

        private void ServerDescription_Expanded(object sender, RoutedEventArgs e)
        {
            WhatServerPlay = (((System.Windows.Controls.Expander)sender).Name).Replace("ServerDescription", string.Empty);

            (this.FindName(WhatServerPlay + "ListOfPlayers") as Expander).IsExpanded = false;

            FlipViewServerBrowser.HideControlButtons();

            Thread GetDescriptionThread = new Thread(new ThreadStart(GetDescriptionServer));
            GetDescriptionThread.IsBackground = true;
            GetDescriptionThread.Start();
        }

        private void ServerDescription_Collapsed(object sender, RoutedEventArgs e)
        {
            FlipViewServerBrowser.ShowControlButtons();
        }

        private void ServerStatus_Click(object sender, RoutedEventArgs e)
        {
            WhatServerPlay = (((System.Windows.Controls.Primitives.ToggleButton)sender).Name).Replace("ServerStatus", string.Empty);

            if ((sender as System.Windows.Controls.Primitives.ToggleButton).IsChecked == true)
            {
                Thread GetServerStatusThread = new Thread(new ThreadStart(GetDescriptionServer));
                GetServerStatusThread.IsBackground = true;
                GetServerStatusThread.Start();
            }
            //else
            //    FlipViewServerBrowser.ShowControlButtons();
        }

        private void GetDescriptionServer()
        {
            try
            {
                HtmlTextBlock.HtmlTextBlock htb = null;
                string TMP = "";
                Dispatcher.Invoke((Action)(() =>
                {
                    htb = this.FindName(WhatServerPlay + "ServerDescriptionHtmlTextBlock") as HtmlTextBlock.HtmlTextBlock;
                    TMP = htb.Html;
                }));
                if (TMP == "")
                {
                    TMP = getTXT_UTF8_File("servers/" + WhatServerPlay + "/description.txt");
                    Dispatcher.Invoke((Action)(() =>
                    {
                        htb.Html = "" + TMP;
                    }));
                }
            }
            catch (ThreadAbortException)
            {
                throw;
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Ошибка: " + ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void GetServerStatus(string Name_Server)
        {
            ServerStatusOn.Add(Name_Server);
            ListBox lb = null;
            //DataGrid dg = null;
            TextBlock tb = null;
            Button bt = null;
            Border br = null;

            try
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    ProgressRingIsActiveCount++;
                    ProgressRing.IsActive = true;

                    bt = this.FindName(Name_Server + "RefreshServerStatus") as Button;
                    bt.IsEnabled = false;
                }));
                //index = ServerStatusOn.IndexOf(ServerStatusOn.Last());
                string tmp = getTXT_ASCII_File("servers/" + Name_Server + "/launch_options.txt");
                string[] tmparr = tmp.Split(new Char[] { ' ' , '=' });
                


                BattlEyeLoginCredentials blc = new BattlEyeLoginCredentials();
                blc.Host = Dns.GetHostAddresses(tmparr[Array.IndexOf(tmparr, "-connect") + 1])[0];
                //b.Host = Dns.GetHostAddresses("127.0.0.1")[0];
                blc.Port = Convert.ToInt16(tmparr[Array.IndexOf(tmparr, "-port") + 1]);
                blc.Password = " ";

                BattlEyeClient bc = new BattlEyeClient(blc);

                if (bc.Connect().ToString() == "InvalidLogin")
                {
                    List<string> Number_of_players = getHTTP("http://heavy-company.ru/servers/" + Name_Server + ".html").Split(new string[] { "<p>", "</p>", "<br />" }, StringSplitOptions.None).OfType<String>().ToList();
                    Number_of_players.RemoveAt(0);
                    Number_of_players.RemoveAt(Number_of_players.Count - 1);
                    Number_of_players.RemoveAt(1);
                    Number_of_players.RemoveAt(1);
                    Number_of_players.RemoveAt(2);

                    List<string> List_of_players = String.Join("\n", Number_of_players).Split(new string[] { "Список игроков:\n" }, StringSplitOptions.None)[1].Split(new Char[] { '\n' }).OfType<String>().ToList();


                    Dispatcher.Invoke((Action)(() =>
                    {
                        br = this.FindName(Name_Server + "FastStatusServer") as Border;

                        br.BorderBrush = System.Windows.Media.Brushes.Lime;

                        lb = this.FindName(Name_Server + "PlayersList") as ListBox;

                        lb.ItemsSource = List_of_players;

                        tb = this.FindName(Name_Server + "NumberOfPlayers") as TextBlock;

                        tb.Text = Number_of_players[0].Replace("Текущее количество игроков: ", string.Empty);

                        tb = this.FindName(Name_Server + "ServerStatus") as TextBlock;

                        tb.Text = "Онлайн";
                        tb.Foreground = System.Windows.Media.Brushes.Lime;
                    }));
                }
                else
                {
                    Dispatcher.Invoke((Action)(() =>
                    {
                        br = this.FindName(Name_Server + "FastStatusServer") as Border;

                        br.BorderBrush = System.Windows.Media.Brushes.Red;

                        tb = this.FindName(Name_Server + "ServerStatus") as TextBlock;

                        tb.Text = "Оффлайн";
                        tb.Foreground = System.Windows.Media.Brushes.Red;
                    }));
                }

                ServerStatusOn.RemoveAt(ServerStatusOn.IndexOf(Name_Server));

                Dispatcher.Invoke((Action)(() =>
                {
                    bt.IsEnabled = true;
                    ProgressRingIsActiveCount--;
                    if (ProgressRingIsActiveCount == 0)
                        ProgressRing.IsActive = false;
                }));
            }
            catch (ThreadAbortException)
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    bt.IsEnabled = true;
                    ProgressRingIsActiveCount--;
                    if (ProgressRingIsActiveCount == 0)
                        ProgressRing.IsActive = false;
                }));
                ServerStatusOn.RemoveAt(ServerStatusOn.IndexOf(Name_Server));
                throw;
            }
            catch (Exception ex)
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    bt.IsEnabled = true;
                    ProgressRingIsActiveCount--;
                    if (ProgressRingIsActiveCount == 0)
                        ProgressRing.IsActive = false;
                }));
                ServerStatusOn.RemoveAt(ServerStatusOn.IndexOf(Name_Server));
                
                System.Windows.MessageBox.Show("Ошибка: " + ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void FastServersStatus()
        {
            for (int i = 0; i < ListBoxServer.Items.Count; i ++)
            {
                string tmp = string.Empty;

                Dispatcher.Invoke((Action)(() =>
                {
                    tmp = (ListBoxServer.Items[i] as ListBoxItem).Name.Replace("ListBoxItem", string.Empty);
                }));

                if (tmp == "arma_3")
                    continue;

                Thread GetServerStatusThread = new Thread(() => GetServerStatus(tmp));
                GetServerStatusThread.IsBackground = true;
                GetServerStatusThread.Start();
            }
        }


        private void RefreshServerStatus_Click(object sender, RoutedEventArgs e)
        {
            string tmp = (ListBoxServer.SelectedItem as ListBoxItem).Name.Replace("ListBoxItem", string.Empty);

            if (ServerStatusOn.IndexOf(tmp) == -1)
            {
                (this.FindName(tmp + "FastStatusServer") as Border).BorderBrush = System.Windows.Media.Brushes.Red;

                (this.FindName(tmp + "ServerStatus") as TextBlock).Text = "-";
                (this.FindName(tmp + "ServerStatus") as TextBlock).Foreground = System.Windows.Media.Brushes.White;

                (this.FindName(tmp + "NumberOfPlayers") as TextBlock).Text = "0/0";

                ListBox lb = this.FindName(tmp + "PlayersList") as ListBox;
                lb = null;

                Thread GetServerStatusThread = new Thread(() => GetServerStatus(tmp));
                GetServerStatusThread.IsBackground = true;
                GetServerStatusThread.Start();
            }
        }

        private void ListBoxServer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string tmp = (ListBoxServer.SelectedItem as ListBoxItem).Name.Replace("ListBoxItem", string.Empty);

            TextBlock tb = this.FindName(tmp + "ServerStatus") as TextBlock;
            if (tb != null)
            {
                if (tb.Text == "-" && ServerStatusOn.IndexOf(tmp) == -1)
                {
                    Thread GetServerStatusThread = new Thread(() => GetServerStatus(tmp));
                    GetServerStatusThread.IsBackground = true;
                    GetServerStatusThread.Start();
                }
            }

            FlipViewServerBrowser.SelectedIndex = ListBoxServer.SelectedIndex;
            
            FlipViewServerBrowser.ShowControlButtons();

            if ((this.FindName(tmp + "ServerDescription") as System.Windows.Controls.Primitives.ToggleButton) != null)
                if ((this.FindName(tmp + "ServerDescription") as System.Windows.Controls.Primitives.ToggleButton).IsChecked == true)
                    (this.FindName(tmp + "ServerDescription") as System.Windows.Controls.Primitives.ToggleButton).IsChecked = false;

            if (OptionsSlidePanel.IsOpen == true)
                OptionsSlidePanel.IsOpen = false;
        }

        private void FlipViewServerBrowser_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //TabControlServerBrowser.SelectedIndex = FlipViewServerBrowser.SelectedIndex;
            ListBoxServer.SelectedIndex = FlipViewServerBrowser.SelectedIndex;

            if (OptionsSlidePanel.IsOpen == true)
                OptionsSlidePanel.IsOpen = false;
        }

        private void ServerPlay_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            //System.Drawing.Image
            //System.Windows.Controls.Image g = sender as System.Windows.Controls.Image;
            WhatServerPlay = (((System.Windows.Controls.Image)sender).Name).Replace("Image", string.Empty);
            WhatModDownload = WhatServerPlay;
            //(this.FindName(WhatServerPlay + "ServerDescription"));
            //if ((this.FindName(WhatServerPlay + "ServerDescription") as System.Windows.Controls.Primitives.ToggleButton).IsChecked == false)
            //{
                Thread Arma = new Thread(new ThreadStart(FastCheckModsInServerGrid));
                Arma.IsBackground = true;
                Arma.Start();
            //}
        }

        private void ServerPlay_Click(object sender, RoutedEventArgs e)
        {
            WhatServerPlay = (((System.Windows.Controls.Button)sender).Name).Replace("Start", string.Empty);
            WhatModDownload = WhatServerPlay;

            Thread Arma = new Thread(new ThreadStart(FastCheckModsInServerGrid));
            Arma.IsBackground = true;
            Arma.Start();
        }

        private void Arma3Play_Click(object sender, RoutedEventArgs e)
        {
            WhatServerPlay = "Arma3";

            Thread Arma = new Thread(new ThreadStart(LaunchArma));
            Arma.IsBackground = true;
            Arma.Start();
        }
        /////////////////////////////////////////////////////////////////
        #endregion

        #region Параметры запуска
        //Грид c параметрами запуска/////////////////////////////////////
        private void UpdateLaunchOptions()
        {
            Dispatcher.Invoke((Action)(() =>
            {
                exThreadsComboBox.SelectedItem = Convert.ToString(Properties.Settings.Default.exThreads);

                CpuCountNumeric.Value = Convert.ToInt16(Properties.Settings.Default.CpuCount);

                MaxMemNumeric.Value = Convert.ToInt16(Properties.Settings.Default.MaxMem);

                MaxVRAMNumeric.Value = Convert.ToInt16(Properties.Settings.Default.MaxVRAM);

                OtherLaunchOptTextBox.Text = Convert.ToString(Properties.Settings.Default.OtherLaunchOpt);
            }));
            //exThreadsComboBox.SelectedItem = Convert.ToString(Properties.Settings.Default.exThreads);

            //CpuCountNumeric.Value = Convert.ToInt16(Properties.Settings.Default.CpuCount);

            //MaxMemNumeric.Value = Convert.ToInt16(Properties.Settings.Default.MaxMem);

            //MaxVRAMNumeric.Value = Convert.ToInt16(Properties.Settings.Default.MaxVRAM);

            //OtherLaunchOptTextBox.Text = Convert.ToString(Properties.Settings.Default.OtherLaunchOpt);

            //List<string> ListOfParameters = new List<string>
            //{
            //    "NoSplash",
            //    "NoPause",
            //    "NoLogs",
            //    "WorldEmpty",
            //    "Window",
            //    "Battleye",
            //    "CpuCountBool",
            //    "MaxMemBool",
            //    "MaxVRAMBool"
            //};

            //NumericUpDown nud = null;

            //for (int i = 0; i < ListOfParameters.Count; i++)
            //{
            //    Dispatcher.Invoke(new Action(() => nud = (this.FindName(ListOfParameters[i].Replace("Bool", "") + "Numeric") as NumericUpDown)));
            //    if (nud != null)
            //    {
            //        if (Convert.ToBoolean(Properties.Settings.Default[ListOfParameters[i]]) == true)
            //        {
            //            Dispatcher.Invoke((Action)(() =>
            //            {
            //                (this.FindName(ListOfParameters[i].Replace("Bool", "")) as CheckBox).IsChecked = true;
            //                (this.FindName(ListOfParameters[i].Replace("Bool", "") + "Numeric") as NumericUpDown).IsEnabled = true;
            //            }));
                        
            //        }
            //        else
            //        {
            //            Dispatcher.Invoke((Action)(() =>
            //            {
            //                (this.FindName(ListOfParameters[i].Replace("Bool", "")) as CheckBox).IsChecked = false;
            //                (this.FindName(ListOfParameters[i].Replace("Bool", "") + "Numeric") as NumericUpDown).IsEnabled = false;
            //            }));
            //        }
            //    }
            //    else
            //    {
            //        if (Convert.ToBoolean(Properties.Settings.Default[ListOfParameters[i]]) == true)
            //        {
            //            Dispatcher.Invoke((Action)(() =>
            //            {
            //                (this.FindName(ListOfParameters[i]) as CheckBox).IsChecked = true;
            //            }));
            //        }
            //        else
            //        {
            //            Dispatcher.Invoke((Action)(() =>
            //            {
            //                (this.FindName(ListOfParameters[i]) as CheckBox).IsChecked = false;
            //            }));
            //        }
            //    }
            //}

            //NoSplash
            if (Properties.Settings.Default.NoSplash == true)
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    NoSplash.IsChecked = true;
                }));
            }
            else
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    NoSplash.IsChecked = false;
                }));
            }

            //NoPause
            if (Properties.Settings.Default.NoPause == true)
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    NoPause.IsChecked = true;
                }));
            }
            else
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    NoPause.IsChecked = false;
                }));
            }

            //NoLogs
            if (Properties.Settings.Default.NoLogs == true)
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    NoLogs.IsChecked = true;
                }));
            }
            else
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    NoLogs.IsChecked = false;
                }));
            }

            //WorldEmpty
            if (Properties.Settings.Default.WorldEmpty == true)
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    WorldEmpty.IsChecked = true;
                }));
            }
            else
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    WorldEmpty.IsChecked = false;
                }));
            }

            //Window
            if (Properties.Settings.Default.Window == true)
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    Window.IsChecked = true;
                }));
            }
            else
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    Window.IsChecked = false;
                }));
            }

            //Battleye
            if (Properties.Settings.Default.Battleye == true)
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    Battleye.IsChecked = true;
                }));
            }
            else
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    Battleye.IsChecked = false;
                }));
            }

            //CpuCountBool
            if (Properties.Settings.Default.CpuCountBool == true)
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    CpuCount.IsChecked = true;
                    CpuCountNumeric.IsEnabled = true;
                    enableHT.IsEnabled = false;
                }));
            }
            else
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    CpuCount.IsChecked = false;
                    CpuCountNumeric.IsEnabled = false;
                }));
            }

            //enableHT
            if (Properties.Settings.Default.enableHT == true)
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    enableHT.IsChecked = true;
                    CpuCount.IsEnabled = false;
                }));
            }
            else
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    enableHT.IsChecked = false;
                }));
            }

            //MaxMemBool
            if (Properties.Settings.Default.MaxMemBool == true)
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    MaxMem.IsChecked = true;
                    MaxMemNumeric.IsEnabled = true;
                }));
            }
            else
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    MaxMem.IsChecked = false;
                    MaxMemNumeric.IsEnabled = false;
                }));
            }

            //MaxVRAMBool
            if (Properties.Settings.Default.MaxVRAMBool == true)
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    MaxVRAM.IsChecked = true;
                    MaxVRAMNumeric.IsEnabled = true;
                }));
            }
            else
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    MaxVRAM.IsChecked = false;
                    MaxVRAMNumeric.IsEnabled = false;
                }));
            }









            //exThreads
            if (Properties.Settings.Default.exThreadsBool == true)
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    exThreads.IsChecked = true;
                    exThreadsComboBox.IsEnabled = true;
                }));
            }
            else
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    exThreads.IsChecked = false;
                    exThreadsComboBox.IsEnabled = false;
                }));
            }

            //OtherLaunchOpt
            if (Properties.Settings.Default.OtherLaunchOptBool == true)
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    OtherLaunchOpt.IsChecked = true;
                    OtherLaunchOptTextBox.IsEnabled = true;
                }));
            }
            else
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    OtherLaunchOpt.IsChecked = false;
                    OtherLaunchOptTextBox.IsEnabled = false;
                }));
            }
        }

        private void LaunchOptionsStandart_CheckedChanged(object sender, RoutedEventArgs e)
        {
            if ((CheckBox)sender == enableHT)
            {
                if (((CheckBox)sender).IsChecked.Value == true)
                    CpuCount.IsEnabled = false;
                else
                    CpuCount.IsEnabled = true;
            }
            Properties.Settings.Default[((CheckBox)sender).Name] = ((CheckBox)sender).IsChecked.Value;
            Properties.Settings.Default.Save();
        }

        private void LaunchOptionsWithNumeric_CheckedChanged(object sender, RoutedEventArgs e)
        {
            if ((CheckBox)sender == CpuCount)
            {
                if (((CheckBox)sender).IsChecked.Value == true)
                    enableHT.IsEnabled = false;
                else
                    enableHT.IsEnabled = true;
            }

            if (((CheckBox)sender as CheckBox).IsChecked.Value == true)
                (this.FindName(((CheckBox)sender).Name + "Numeric") as NumericUpDown).IsEnabled = true;
            else
                (this.FindName(((CheckBox)sender).Name + "Numeric") as NumericUpDown).IsEnabled = false;

            Properties.Settings.Default[((CheckBox)sender).Name + "Bool"] = ((CheckBox)sender as CheckBox).IsChecked.Value;
            Properties.Settings.Default.Save();
        }

        private void exThreads_CheckedChanged(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.exThreadsBool = exThreads.IsChecked.Value;

            if (exThreads.IsChecked.Value == true)
                exThreadsComboBox.IsEnabled = true;
            else
                exThreadsComboBox.IsEnabled = false;

            Properties.Settings.Default.Save();
        }

        private void OtherLaunchOpt_CheckedChanged(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.OtherLaunchOptBool = OtherLaunchOpt.IsChecked.Value;

            if (OtherLaunchOpt.IsChecked == true)
                OtherLaunchOptTextBox.IsEnabled = true;
            else
                OtherLaunchOptTextBox.IsEnabled = false;

            Properties.Settings.Default.Save();
        }

        private void LaunchOptionsNumeric_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double?> e)
        {
            Properties.Settings.Default[(((NumericUpDown)sender).Name).Replace("Numeric", "")] = Convert.ToInt32(((NumericUpDown)sender as NumericUpDown).Value);
            Properties.Settings.Default.Save();
        }

        private void exThreadsComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Properties.Settings.Default.exThreads = Convert.ToInt16(exThreadsComboBox.SelectedItem);
            Properties.Settings.Default.Save();
        }

        private void OtherLaunchOptTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            Properties.Settings.Default.OtherLaunchOpt = Convert.ToString(OtherLaunchOptTextBox.Text);
            Properties.Settings.Default.Save();
        }
        /////////////////////////////////////////////////////////////////
        #endregion

        #region Моды
        //Грид с модами//////////////////////////////////////////////////
        public void OnClosedDownload(object sender, EventArgs e)
        {
            Button b = this.FindName(WhatModDownload + "ModDownload") as Button;
            TextBlock tb = this.FindName(WhatModDownload + "ModStatus") as TextBlock;

            if (WhatModDownload == "arma_3")
                Update_arma.IsChecked = true;
            else
                Mods.IsChecked = true;

            Properties.Settings.Default.Save();

            Visibility = Visibility.Visible;

            if (DownloadWindow.Finish == false)
            {
                b.IsEnabled = false;
                tb.Text = "Статус: Загрузка не завершена";
                tb.Foreground = System.Windows.Media.Brushes.Red;
            }
            else
            {
                b.IsEnabled = false;
                tb.Text = "Статус: Загрузка завершена";
                tb.Foreground = System.Windows.Media.Brushes.Lime;

                var fileversioninfo = FileVersionInfo.GetVersionInfo(Properties.Settings.Default.ArmaPath);
                if (WhatModDownload == "arma_3")
                {
                    (this.FindName(WhatModDownload + "LocalVersion") as TextBlock).Text = fileversioninfo.FileMajorPart.ToString() + "." + fileversioninfo.FileMinorPart.ToString() + "." + fileversioninfo.FileBuildPart.ToString() + "." + fileversioninfo.FilePrivatePart.ToString();

                    Thread Update_armaThread = new Thread(new ThreadStart(Update_arma_Function));
                    Update_armaThread.IsBackground = true;
                    Update_armaThread.Start();
                }
                else
                {
                    (this.FindName(WhatModDownload + "LocalVersion") as TextBlock).Text = File.ReadAllText(Properties.Settings.Default.ArmaPath.Replace("arma3.exe", "") + "version_of_mod\\" + WhatModDownload + ".txt").Replace(" ", "");

                    Thread GetDescriptionThread = new Thread(new ThreadStart(GetDescriptionMod));
                    GetDescriptionThread.IsBackground = true;
                    GetDescriptionThread.Start();
                }

                System.Windows.MessageBox.Show("Загрузка завершена", "Загрузка завершена", MessageBoxButton.OK, MessageBoxImage.Information);
            }

            DownloadWindow = null;
        }

        private void CheckMods_Function()
        {
            try
            {
                TextBlock tb = null;
                Button b = null;

                FastUpdateMod = true;
                UpdateMod = false;

                Dispatcher.Invoke((Action)(() =>
                {
                    tb = this.FindName(WhatModDownload + "ModStatus") as TextBlock;

                    tb.Text = "Статус: Идет проверка...";
                    tb.Foreground = System.Windows.Media.Brushes.White;

                    for (int i = 0; i < ModsStackPanel.Children.Count; i++)
                    {
                        Expander exp = ModsStackPanel.Children[i] as Expander;

                        b = this.FindName((exp.Name).Replace("Mod", "") + "ModDownload") as Button;
                        b.IsEnabled = false;

                        if (exp.IsExpanded == false)
                            exp.IsEnabled = false;
                    }

                    if(WhatModDownload == "arma_3")
                    {
                        b = this.FindName(WhatModDownload + "ModDownload") as Button;
                        b.IsEnabled = false;
                    }

                    b = this.FindName(WhatModDownload + "ModCheck") as Button;
                    b.IsEnabled = false;

                    b = this.FindName(WhatModDownload + "FastModCheck") as Button;
                    b.IsEnabled = false;

                    b = this.FindName(WhatModDownload + "ModCheckAbort") as Button;
                    b.IsEnabled = true;


                    b = this.FindName(WhatModDownload + "ModDelete") as Button;
                    if(b != null)
                        b.IsEnabled = false;
                }));

                CheckMods();

                Dispatcher.Invoke((Action)(() =>
                {
                    b = this.FindName(WhatModDownload + "ModCheck") as Button;
                    b.IsEnabled = true;

                    b = this.FindName(WhatModDownload + "FastModCheck") as Button;
                    b.IsEnabled = true;

                    b = this.FindName(WhatModDownload + "ModCheckAbort") as Button;
                    b.IsEnabled = false;

                    b = this.FindName(WhatModDownload + "ModDelete") as Button;
                    if (b != null)
                        b.IsEnabled = true;

                    for (int i = 0; i < ModsStackPanel.Children.Count; i++)
                    {
                        Expander exp = ModsStackPanel.Children[i] as Expander;

                        exp.IsEnabled = true;
                    }

                    b = this.FindName(WhatModDownload + "ModDownload") as Button;
                    if (tb.Text != "Статус: Загрузка завершена" && tb.Text != "Статус: Загрузка не завершена")
                    {
                        if (UpdateMod == false)
                        {
                            b.IsEnabled = false;
                            tb.Text = "Статус: Нет обновления";
                            tb.Foreground = System.Windows.Media.Brushes.Lime;
                        }
                        else
                        {
                            b.IsEnabled = true;
                            tb.Text = "Статус: Есть обновление";
                            tb.Foreground = System.Windows.Media.Brushes.Red;
                        }
                    }

                    for (int i = 0; i < ModsStackPanel.Children.Count; i++)
                    {
                        Expander exp = ModsStackPanel.Children[i] as Expander;
                        exp.IsEnabled = true;
                    }
                }));
            }
            catch (ThreadAbortException)
            {
                throw;
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Ошибка: " + ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void FastCheckMods_Function()
        {
            try
            {
                TextBlock tb = null;
                Button b = null;

                UpdateMod = false;

                Dispatcher.Invoke((Action)(() =>
                {
                    tb = this.FindName(WhatModDownload + "ModStatus") as TextBlock;

                    tb.Text = "Статус: Идет проверка...";
                    tb.Foreground = System.Windows.Media.Brushes.White;

                    for (int i = 0; i < ModsStackPanel.Children.Count; i++)
                    {
                        Expander exp = ModsStackPanel.Children[i] as Expander;
                        if (exp.IsExpanded == false)
                            exp.IsEnabled = false;
                    }


                    b = this.FindName(WhatModDownload + "ModCheck") as Button;
                    b.IsEnabled = false;

                    b = this.FindName(WhatModDownload + "FastModCheck") as Button;
                    b.IsEnabled = false;

                    b = this.FindName(WhatModDownload + "ModDownload") as Button;
                    b.IsEnabled = false;

                    b = this.FindName(WhatModDownload + "ModDelete") as Button;
                    if (b != null)
                        b.IsEnabled = false;
                }));

                FastCheckMods();

                Dispatcher.Invoke((Action)(() =>
                {
                    b = this.FindName(WhatModDownload + "ModCheck") as Button;
                    b.IsEnabled = true;

                    b = this.FindName(WhatModDownload + "FastModCheck") as Button;
                    b.IsEnabled = true;

                    b = this.FindName(WhatModDownload + "ModDelete") as Button;
                    if (b != null)
                        b.IsEnabled = true;

                    if (tb.Text != "Статус: Загрузка завершена" && tb.Text != "Статус: Загрузка прервана")
                    {
                        if (FastUpdateMod != false)
                        {
                            b = this.FindName(WhatModDownload + "ModDownload") as Button;
                            if (UpdateMod == false)
                            {
                                b.IsEnabled = false;

                                tb.Text = "Статус: Нет обновления";
                                tb.Foreground = System.Windows.Media.Brushes.Lime;

                                //Properties.Settings.Default.FirstCheckEpoch = true;
                            }
                            else
                            {
                                b.IsEnabled = true;
                                tb.Text = "Статус: Есть обновление";
                                tb.Foreground = System.Windows.Media.Brushes.Red;

                                //Properties.Settings.Default.FirstCheckEpoch = false;
                            }
                        }
                        else
                        {
                            tb.Text = "Статус:";
                            tb.Foreground = System.Windows.Media.Brushes.White;
                        }
                    }  

                    for (int i = 0; i < ModsStackPanel.Children.Count; i++)
                    {
                        Expander exp = ModsStackPanel.Children[i] as Expander;
                        exp.IsEnabled = true;
                    }
                }));
            }
            catch (ThreadAbortException)
            {
                throw;
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Ошибка: " + ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DeleteMods_Function()
        {
            try
            {
                TextBlock tb = null;
                Button b = null;

                Dispatcher.Invoke((Action)(() =>
                {
                    tb = this.FindName(WhatModDownload + "ModStatus") as TextBlock;

                    tb.Text = "Статус: Идет удаление...";
                    tb.Foreground = System.Windows.Media.Brushes.White;

                    for (int i = 0; i < ModsStackPanel.Children.Count; i++)
                    {
                        Expander exp = ModsStackPanel.Children[i] as Expander;
                        if (exp.IsExpanded == false)
                            exp.IsEnabled = false;
                    }


                    b = this.FindName(WhatModDownload + "ModCheck") as Button;
                    b.IsEnabled = false;

                    b = this.FindName(WhatModDownload + "FastModCheck") as Button;
                    b.IsEnabled = false;

                    b = this.FindName(WhatModDownload + "ModDownload") as Button;
                    b.IsEnabled = false;

                    b = this.FindName(WhatModDownload + "ModDelete") as Button;
                    b.IsEnabled = false;
                }));

                List<string> folders_to_delete = getTXT_ASCII_File("mods/" + WhatModDownload + "/folders_to_delete.txt").Split(new Char[] { '\n' }).ToList();

                //for (int i = 0; i < folders_to_delete.Count; i++)
                //{
                //    folders_to_delete[i] = folders_to_delete[i].Replace("\r", string.Empty);
                //}

                bool mod_delete = false;

                for (int i = 0; i < folders_to_delete.Count; i++)
                {
                    if (Directory.Exists(Properties.Settings.Default.ArmaPath.Replace("arma3.exe", "") + folders_to_delete[i]) && folders_to_delete[i] != string.Empty)
                    {
                        Directory.Delete(Properties.Settings.Default.ArmaPath.Replace("arma3.exe", "") + folders_to_delete[i], true);

                        mod_delete = true;
                    }
                        
                }

                if (File.Exists(Properties.Settings.Default.ArmaPath.Replace("arma3.exe", "") + "version_of_mod\\" + WhatModDownload + ".txt"))
                    File.Delete(Properties.Settings.Default.ArmaPath.Replace("arma3.exe", "") + "version_of_mod\\" + WhatModDownload + ".txt");

                Dispatcher.Invoke((Action)(() =>
                {
                    tb = this.FindName(WhatModDownload + "LocalVersion") as TextBlock;
                    tb.Text = "-";

                    tb = this.FindName(WhatModDownload + "LocalSize") as TextBlock;
                    tb.Text = "-";

                    tb = this.FindName(WhatModDownload + "ModStatus") as TextBlock;

                    for (int i = 0; i < ModsStackPanel.Children.Count; i++)
                    {
                        Expander exp = ModsStackPanel.Children[i] as Expander;
                        exp.IsEnabled = true;
                    }

                    b = this.FindName(WhatModDownload + "ModCheck") as Button;
                    b.IsEnabled = true;

                    b = this.FindName(WhatModDownload + "FastModCheck") as Button;
                    b.IsEnabled = true;

                    b = this.FindName(WhatModDownload + "ModDelete") as Button;
                    b.IsEnabled = true;
                }));



                if (folders_to_delete.Count != 0 && mod_delete == true)
                {
                    Dispatcher.Invoke((Action)(() =>
                    {
                        tb.Text = "Статус: Мод удален";
                        tb.Foreground = System.Windows.Media.Brushes.Lime;
                    }));

                    System.Windows.MessageBox.Show("Мод был успешно удален", "", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    Dispatcher.Invoke((Action)(() =>
                    {
                        tb.Text = "Статус:";
                        tb.Foreground = System.Windows.Media.Brushes.White;
                    }));

                    System.Windows.MessageBox.Show("Вы не установили мод", "", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (ThreadAbortException)
            {
                throw;
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Ошибка: " + ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void GetDescriptionMod()
        {
            try
            {
                string temp = WhatModDownload;
                HtmlTextBlock.HtmlTextBlock htb = null;
                string TMP = "";
                Dispatcher.Invoke((Action)(() =>
                {
                    htb = this.FindName(temp + "ModDescription") as HtmlTextBlock.HtmlTextBlock;
                    TMP = htb.Html;
                }));
                if (TMP == "")
                {
                    TMP = getTXT_UTF8_File("mods/" + temp + "/Description.txt");
                    Dispatcher.Invoke((Action)(() =>
                    {
                        htb.Html = "" + TMP;
                    }));
                }

                List<string> list = getFTP_ListDirectory("mods/" + temp + "/Mod/");
                long modsize = 0;

                for (int i = 0; i < list.Count; i++)
                {
                    if (Directory.Exists(Properties.Settings.Default.ArmaPath.Replace("arma3.exe", "") + list[i]))
                        modsize += GetDirectorySize(Properties.Settings.Default.ArmaPath.Replace("arma3.exe", "") + list[i]);
                }

                Dispatcher.Invoke((Action)(() =>
                {
                    if ((Math.Round(modsize / 1048576.0, 2) > 1024.0))
                    {
                        if (Math.Round(modsize / 1073741824.0, 2) <= 0)
                            (this.FindName(temp + "LocalSize") as TextBlock).Text = "-";
                        else
                            (this.FindName(temp + "LocalSize") as TextBlock).Text = Math.Round(modsize / 1073741824.0, 2) + " GB";
                    }
                    else
                    {
                        if (Math.Round(modsize / 1048576.0, 2) <= 0)
                            (this.FindName(temp + "LocalSize") as TextBlock).Text = "-";
                        else
                            (this.FindName(temp + "LocalSize") as TextBlock).Text = Math.Round(modsize / 1048576.0, 2) + " MB";
                    }
                }));
            }
            catch (ThreadAbortException)
            {
                throw;
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Ошибка: " + ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }


        private void ExpanderMod_Expanded(object sender, RoutedEventArgs e)
        {
            //if (Directory.Exists(Properties.Settings.Default.ArmaPath.Replace("arma3.exe", "") + "@Epoch"))
            //    EpochModDelete.IsEnabled = true;
            //else
            //    EpochModDelete.IsEnabled = false;

            WhatModDownload = (((Expander)sender).Name).Replace("Mod", "");

            if (File.Exists(Properties.Settings.Default.ArmaPath.Replace("arma3.exe", "") + "version_of_mod\\" + WhatModDownload + ".txt"))
                (this.FindName(WhatModDownload + "LocalVersion") as TextBlock).Text = File.ReadAllText(Properties.Settings.Default.ArmaPath.Replace("arma3.exe", "") + "version_of_mod\\" + WhatModDownload + ".txt").Replace(" ", "");
            else
                (this.FindName(WhatModDownload + "LocalVersion") as TextBlock).Text = "-";

            //(this.FindName((TabControlServerBrowser.SelectedItem as TabItem).Name.Replace("TabItem", "") + "ServerDescription") as System.Windows.Controls.Primitives.ToggleButton).IsChecked = false;

            //WhatModDownload = "Epoch";

            Thread GetDescriptionThread = new Thread(new ThreadStart(GetDescriptionMod));
            GetDescriptionThread.IsBackground = true;
            GetDescriptionThread.Start();
        }

        private void ModCheck_Click(object sender, RoutedEventArgs e)
        {
            WhatModDownload = (((Button)sender).Name).Replace("ModCheck", "");

            Check = new Thread(new ThreadStart(CheckMods_Function));
            Check.IsBackground = true;
            Check.Start();
        }

        private void FastModCheck_Click(object sender, RoutedEventArgs e)
        {
            WhatModDownload = (((Button)sender).Name).Replace("FastModCheck", "");

            Thread FastCheck = new Thread(new ThreadStart(FastCheckMods_Function));
            FastCheck.IsBackground = true;
            FastCheck.Start();
        }

        private void ModDownload_Click(object sender, RoutedEventArgs e)
        {
            WhatModDownload = (((Button)sender).Name).Replace("ModDownload", "");
            Visibility = Visibility.Hidden;

            DownloadWindow = new DownloadWindow();
            DownloadWindow.Closed += OnClosedDownload;
            DownloadWindow.ShowDialog();

            //WhatModDownload = "Epoch";
            //Visibility = Visibility.Hidden;
            //DownloadWindow = new DownloadWindow();
            //DownloadWindow.Owner = this;

            //DownloadIsActiveWindow = true;

            //DownloadWindow.Closed += OnClosedDownload;
            //DownloadWindow.ShowDialog();

            //if (DownloadWindow.IsInitialized == false)
            //{
            //    DownloadWindow = null;

            //    //DownloadIsActiveWindow = false;

            //    DownloadEpochMods.IsEnabled = false;
            //    StatusEpoch.Content = "Статус: Загрузка завершена";
            //    StatusEpoch.Foreground = System.Windows.Media.Brushes.Lime;

            //    Properties.Settings.Default.FirstCheckEpoch = true;

            //    LaunchOptionsGrid.Visibility = Visibility.Hidden;
            //    ChooseServerGrid.Visibility = Visibility.Hidden;
            //    ModsGrid.Visibility = Visibility.Visible;

            //    Properties.Settings.Default.Save();

            //    Visibility = Visibility.Visible;
            //}

            //Thread ABC = new Thread(new ThreadStart(Test));
            //ABC.IsBackground = true;
            //ABC.Start();
            //Test();
        }

        private void ModCheckAbort_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = System.Windows.MessageBox.Show("Вы точно хотите остановить проверку?", "", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                Check.Abort();

                ChangePathArma.IsEnabled = true;
                CheckLauncher.IsEnabled = true;
                ChooseServer.IsEnabled = true;
                Update_arma.IsEnabled = true;
                Mods.IsEnabled = true;

                if (WhatModDownload == "arma_3")
                    Update_arma.IsChecked = true;
                else
                    Mods.IsChecked = true;
                //ModsGrid.Visibility = Visibility.Visible;

                this.Topmost = true;
                this.Focus();
                this.Topmost = false;

                TextBlock tb = this.FindName(WhatModDownload + "ModStatus") as TextBlock;

                (this.FindName(WhatModDownload + "ModCheck") as Button).IsEnabled = true;

                (this.FindName(WhatModDownload + "FastModCheck") as Button).IsEnabled = true;

                (this.FindName(WhatModDownload + "ModCheckAbort") as Button).IsEnabled = false;

                if ((this.FindName(WhatModDownload + "ModDelete") as Button) != null)
                    (this.FindName(WhatModDownload + "ModDelete") as Button).IsEnabled = true;

                (this.FindName(WhatModDownload + "ModDownload") as Button).IsEnabled = false;
                tb.Text = "Статус: Проверка прервана";
                tb.Foreground = System.Windows.Media.Brushes.Red;

                for (int i = 0; i < ModsStackPanel.Children.Count; i++)
                {
                    Expander exp = ModsStackPanel.Children[i] as Expander;
                    exp.IsEnabled = true;
                }
            }
        }

        private void ModDelete_Click(object sender, RoutedEventArgs e)
        {
            WhatModDownload = (((Button)sender).Name).Replace("ModDelete", "");

            MessageBoxResult result = System.Windows.MessageBox.Show("Вы точно хотите удалить мод?", "", MessageBoxButton.YesNo);
            if (result == MessageBoxResult.Yes)
            {
                Thread Delete = new Thread(new ThreadStart(DeleteMods_Function));
                Delete.IsBackground = true;
                Delete.Start();
            }
        }
        /////////////////////////////////////////////////////////////////
        #endregion

        private void LaunchArma()
        {
            try
            {
                string OptLaunch = "";

                if(Properties.Settings.Default.Battleye == true)
                    OptLaunch = "0 1";
                if (Properties.Settings.Default.OtherLaunchOptBool == true && WhatServerPlay == "Arma3")
                    OptLaunch += " " + Properties.Settings.Default.OtherLaunchOpt;
                if (Properties.Settings.Default.NoSplash == true)
                    OptLaunch += " -noSplash";
                if (Properties.Settings.Default.NoPause == true)
                    OptLaunch += " -noPause";
                if (Properties.Settings.Default.NoLogs == true)
                    OptLaunch += " -noLogs";
                if (Properties.Settings.Default.WorldEmpty == true)
                    OptLaunch += " -world=Empty";
                if (Properties.Settings.Default.Window == true)
                    OptLaunch += " -window";
                if (Properties.Settings.Default.enableHT == true)
                    OptLaunch += " -enableHT";
                if (Properties.Settings.Default.CpuCountBool == true)
                    OptLaunch += " -cpuCount=" + Properties.Settings.Default.CpuCount;
                if (Properties.Settings.Default.exThreadsBool == true)
                    OptLaunch += " -exThreads=" + Properties.Settings.Default.exThreads;
                if (Properties.Settings.Default.MaxMemBool == true)
                    OptLaunch += " -maxMem=" + Properties.Settings.Default.MaxMem;
                if (Properties.Settings.Default.MaxVRAMBool == true)
                    OptLaunch += " -maxVRAM=" + Properties.Settings.Default.MaxVRAM;

                if (WhatServerPlay != "Arma3")
                    OptLaunch += getTXT_ASCII_File("servers/" + WhatServerPlay + "/launch_options.txt");


                proc.StartInfo.UseShellExecute = true;
                if (Properties.Settings.Default.Battleye == true)
                    proc.StartInfo.FileName = (Properties.Settings.Default.ArmaPath.Replace("arma3.exe", "") + "arma3battleye.exe");
                else
                    proc.StartInfo.FileName = (Properties.Settings.Default.ArmaPath);
                proc.StartInfo.Arguments = OptLaunch;
                proc.Start();

                Dispatcher.Invoke((Action)(() =>
                {
                    this.BlockLauncher.Visibility = Visibility.Visible;
                    HCMarkBorder.Visibility = Visibility.Hidden;
                    VersionOfLauncherBorder.Visibility = Visibility.Hidden;

                    Visibility = Visibility.Hidden;
                    //this.ShowInTaskbar = false;
                    //this.Hide();

                    
                    //TaskbarIcon tbi = new TaskbarIcon();
                    //tbi.IconSource = new BitmapImage(new Uri("pack://application:,,,/Resources/General_ico.ico", UriKind.RelativeOrAbsolute));
                    //tbi.ToolTipText = "hello world";

                    //Trey.Visibility = Visibility.Visible;
                }));

                proc.WaitForExit();

                Dispatcher.Invoke((Action)(() =>
                {
                    this.BlockLauncher.Visibility = Visibility.Hidden;
                    HCMarkBorder.Visibility = Visibility.Visible;
                    VersionOfLauncherBorder.Visibility = Visibility.Visible;

                    Visibility = Visibility.Visible;

                    this.Topmost = true;
                    this.Focus();
                    this.Topmost = false;

                    //this.ShowInTaskbar = true;
                    //this.ShowDialog();

                    //Trey.IsEnabled = false;
                    //Trey.Visibility = Visibility.Hidden;
                }));
            }
            catch (ThreadAbortException)
            {
                throw;
            }
            catch (System.ComponentModel.Win32Exception ex)
            {
                MessageBoxResult result = new MessageBoxResult();
                if (Properties.Settings.Default.Battleye == true)
                    result = System.Windows.MessageBox.Show("Ошибка: " + ex.Message + ": \n" + Properties.Settings.Default.ArmaPath.Replace("arma3.exe", "") + "arma3battleye.exe" + "\n" + "Хотите изменить путь сейчас?", "Ошибка", MessageBoxButton.YesNo, MessageBoxImage.Error);
                else
                    result = System.Windows.MessageBox.Show("Ошибка: " + ex.Message + ": \n" + Properties.Settings.Default.ArmaPath + "\n" + "Хотите изменить путь сейчас?", "Ошибка", MessageBoxButton.YesNo, MessageBoxImage.Error);
                if (result == MessageBoxResult.Yes)
                {
                    Microsoft.Win32.OpenFileDialog OPF = new Microsoft.Win32.OpenFileDialog();
                    OPF.Filter = "Arma 3|arma3.exe";

                    if (OPF.ShowDialog() == true)
                    {
                        Properties.Settings.Default.ArmaPath = OPF.FileName;
                        Properties.Settings.Default.Save();
                    }
                }
                //System.Windows.MessageBox.Show("Ошибка: " + ex.Message + ": \n" + Properties.Settings.Default.ArmaPath, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Ошибка: " + ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

    }

    public class DispatcherHelper
    {
        private static DispatcherOperationCallback exitFrameCallback = new
             DispatcherOperationCallback(ExitFrame);

        /// <summary>
        /// Processes all UI messages currently in the message queue.
        /// </summary>
        public static void DoEvents()
        {
            // Create new nested message pump.
            DispatcherFrame nestedFrame = new DispatcherFrame();

            // Dispatch a callback to the current message queue, when getting called,
            // this callback will end the nested message loop.
            // note that the priority of this callback should be lower than that of UI event messages.
            DispatcherOperation exitOperation = Dispatcher.CurrentDispatcher.BeginInvoke(
                DispatcherPriority.Background, exitFrameCallback, nestedFrame);

            // pump the nested message loop, the nested message loop will immediately
            // process the messages left inside the message queue.
            Dispatcher.PushFrame(nestedFrame);

            // If the "exitFrame" callback is not finished, abort it.
            if (exitOperation.Status != DispatcherOperationStatus.Completed)
            {
                exitOperation.Abort();
            }
        }

        private static Object ExitFrame(Object state)
        {
            DispatcherFrame frame = state as DispatcherFrame;

            // Exit the nested message loop.
            frame.Continue = false;
            return null;
        }
    }
}