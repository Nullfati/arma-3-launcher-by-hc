﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Threading;
using System.Net;
using System.IO;

namespace Arma_3_Launcher_by_HC
{
    /// <summary>
    /// Логика взаимодействия для Changelog.xaml
    /// </summary>
    public partial class ChangelogWindow : Window
    {
        private string FTP_USER = "launcher";
        private string FTP_PASSWORD = "flAjzm1eCj";

        public ChangelogWindow()
        {
            InitializeComponent();
        }

        private string getTXT_UTF8_File(string PathToFile)
        {
            Dispatcher.Invoke((Action)(() =>
            {
                ProgressRing.IsActive = true;
            }));

            // Объект запроса
            FtpWebRequest rew = (FtpWebRequest)WebRequest.Create("ftp://62.109.13.179/" + PathToFile);
            rew.Timeout = 600000;
            rew.Credentials = new NetworkCredential(FTP_USER, FTP_PASSWORD);
            rew.KeepAlive = false;

            // Отправить запрос и получить ответ
            //FtpWebResponse resp = null;

            // Получить поток
            Stream str = new MemoryStream();

            try
            {
                FtpWebResponse resp = (FtpWebResponse)rew.GetResponse();
                str = resp.GetResponseStream();

                List<byte> newFileData = new List<byte>();

                for (int ch; ; )
                {
                    ch = str.ReadByte();
                    if (ch == -1) break;
                    newFileData.Add(Convert.ToByte(ch));
                    //test[i - 1] = ch;
                }

                str.Close();
                resp.Close();

                Dispatcher.Invoke((Action)(() =>
                {
                    ProgressRing.IsActive = false;
                }));

                return System.Text.Encoding.UTF8.GetString(newFileData.ToArray());

            }
            catch (ThreadAbortException)
            {
                str.Close();
                //resp.Close();

                Dispatcher.Invoke((Action)(() =>
                {
                    ProgressRing.IsActive = false;
                }));

                return string.Empty;
            }
            catch (Exception ex)
            {
                str.Close();
                //resp.Close();

                Dispatcher.Invoke((Action)(() =>
                {
                    ProgressRing.IsActive = false;
                }));

                System.Windows.MessageBox.Show("Ошибка: " + ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return string.Empty;
            }
        }

        private void GetChangelog()
        {
            HtmlTextBlock.HtmlTextBlock htb = null;
            string TMP = "";
            Dispatcher.Invoke((Action)(() =>
            {
                htb = this.FindName("Test") as HtmlTextBlock.HtmlTextBlock;
                TMP = htb.Html;
            }));
            if (TMP == "")
            {
                TMP = getTXT_UTF8_File("launcher/changelog.txt");
                Dispatcher.Invoke((Action)(() =>
                {
                    htb.Html = "" + TMP;
                }));
            }
        }

        private void DragWindow_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void CloseButton_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Thread GetChangelogThread = new Thread(new ThreadStart(GetChangelog));
            GetChangelogThread.IsBackground = true;
            GetChangelogThread.Start();
        }
    }
}
